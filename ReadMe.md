# Windfarm Occlusion Python Application
This Python project originates from a bachelor thesis: Occlusion for Augmented Reality 
Windfarms in Great Distance from January 2023. It is one of two code projects of a developed 
prototype to evaluate different computer vision algorithms along topographical data 
to solve the occlusion problem for wind farm visualization at great distances. It was 
developed to evaluate a Dataset which was created using the Unity prototype.

## Requirements
- DenseDepth git repository: `git clone https://github.com/ialhashim/DenseDepth.git`
- DenseDepth Neural Network: `!wget https://s3-eu-west-1.amazonaws.com/densedepth/nyu.h5 -O ./DenseDepth/nyu.h5`

### Conda environment:
`$ conda create -n depthEnv jupyter tensorflow-gpu matplotlib pillow pandas keras scikit-learn scikit-image pydot tqdm seaborn`

`$ conda activate depthEnv`

`$ conda install -c conda-forge opencv`



## Folder Sturcture:
- Dataset: Contains the Visualization of the dataset and its occlusion results.
- FullPaulusstr: Contains the Scenes that were captured from the location Paulusstr 
that were used to compute the visualizaiton of the Dataset folder.
- Informatikzentrum: Contains the Scenes that were captured from the location Informatikzentrum 
that were used to compute the visualizaiton of the Dataset folder.
- Koenigshuegel: Contains the Scenes that were captured from the location Königshügel 
that were used to compute the visualizaiton of the Dataset folder.
- Lousberg: Contains the Scenes that were captured from the location Lousberg 
that were used to compute the visualizaiton of the Dataset folder.

- Handler: Contains all python files and classes.
- Presentation: Contains some jupyter notebooks that were used to create visualizations for 
the bachelor defense.


## Project Structure
- OcclusionHandler: Central point of this project. Uses the FileHandler, 
SfMHandler, SkyHandler and MDEHandler to provide all needed occlusion maps and also to provide 
the occlusions.
  - FileHandler: Loads all needed files and adjusts them accordingly and providing the 
  OcclusionHandler access to those files.
    - RotationHandler: Used by the FileHandler to rotate and cut the images as needed.
  - SfMHandler: Provides OcclusionHandler with the SfM occlusion map.
  - SkyHandler: Provides OcclusionHandler with the Sky occlusion map.
  - MDEHandler: Provides OcclusionHandler with the MDE occlusion map.

- utils: Provides different utility functions.
- AnalysisHandler: Is a wrapper around the OcclusionHandler, and can be used to compute different 
metrics of a Scene.
- SequenceAnalysisHandler: Is a wrapper around the OcclusionHandler, and uses the AnalysisHandler 
to compute different of all Scenes for an entire Sequence.

## Standard Procedure
OcclusionHandler is initialized for a specified Sequence. With load_scene a secified Scene is 
loaded. During that the FileHandler is used to load all data from the specified Scene and 
adjusting it, using the RotationHandler to also provide the rotated and cutout images. After 
that the OcclusionHandler uses the data provided by the FileHandler to initialize the SfMHandler, 
the SkyHandler and the MDEHandler. Now the different occlusion maps can be accessed using 
the OcclusionHandler.