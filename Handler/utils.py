import math

import numpy as np
from enum import Enum, IntEnum
import cv2
from PIL import Image

"""
Implements different utility functions that are used in all python files and jupyter notebooks.
"""

blue = [255, 0, 0]
red = [0, 0, 255]
cyan = [139, 139, 0]
white = [255, 255, 255]
black = [0, 0, 0]


################## Enums ##############################################
class ImageVariant(IntEnum):
    FULL = 0
    SQUARE = 1
    ROTATED = 2
    CUTOUT = 3


class MDEThresholdMode(Enum):
    MAX = 0
    UF = 1
    MIN_UF_MAX = 2
    QUARTILE3 = 3
    HAND = 4


class ConcatMode(IntEnum):
    VERT = 0
    HORIZONTAL = 1


class FineTuneMode(Enum):
    SFM = 0
    MDE = 1


################## Vector Arithmetic ##############################################
class Vec2D:
    """
    Used for different vector arithmetic in combination with the wind turbine points.
    """
    def __init__(self, x: float, y: float):
        self.x = x
        self.y = y

    def norm(self):
        return np.sqrt(self.x ** 2 + self.y ** 2)

    def copy(self):
        return Vec2D(x=self.x, y=self.y)

    def normalized(self):
        norm = self.norm()
        return Vec2D(x=self.x / norm, y=self.y / norm)

    @staticmethod
    def calculate_angular_between_vecs(vec1, vec2):
        vec1 = np.array([[vec1.x], [vec1.y]])
        vec2 = np.array([[vec2.x], [vec2.y]])
        return np.arccos(np.dot(vec1, vec2) / (np.norm(vec1) + np.norm(vec2)))

    def convert_to_Vec2dInt(self):
        return Vec2DInt(x=self.x, y=self.y)

    def __add__(self, scalar_or_vec2d):
        if not isinstance(scalar_or_vec2d, Vec2D): scalar_or_vec2d = Vec2D(scalar_or_vec2d, scalar_or_vec2d)
        return Vec2D(self.x + scalar_or_vec2d.x, self.y + scalar_or_vec2d.y)

    def __radd__(self, scalar_or_vec2d):
        return self.__add__(scalar_or_vec2d)

    def __sub__(self, scalar_or_vec2d):
        if not isinstance(scalar_or_vec2d, Vec2D): scalar_or_vec2d = Vec2D(scalar_or_vec2d, scalar_or_vec2d)
        return Vec2D(self.x - scalar_or_vec2d.x, self.y - scalar_or_vec2d.y)

    def __rsub__(self, scalar_or_vec2d):
        return self.__sub__(scalar_or_vec2d)

    def __mul__(self, scalar_or_vec2d):
        if not isinstance(scalar_or_vec2d, Vec2D): scalar_or_vec2d = Vec2D(scalar_or_vec2d, scalar_or_vec2d)
        return Vec2D(self.x * scalar_or_vec2d.x, self.y * scalar_or_vec2d.y)

    def __rmul__(self, scalar_or_vec2d):
        return self.__mul__(scalar_or_vec2d)

    def __truediv__(self, scalar_or_vec2d):
        if not isinstance(scalar_or_vec2d, Vec2D): scalar_or_vec2d = Vec2D(scalar_or_vec2d, scalar_or_vec2d)
        return Vec2D(self.x / scalar_or_vec2d.x, self.y / scalar_or_vec2d.y)

    def __rtruediv__(self, scalar_or_vec2d):
        return self.__truediv__(scalar_or_vec2d)

    def __str__(self):
        return f"Vec2D(x={self.x}, y={self.y})"

    def __repr__(self):
        return f"Vec2D(x={self.x}, y={self.y})"


class Vec2DInt(Vec2D):
    """
    Subclass from Vec2D for integer vector arithmetic.
    """
    def __init__(self, x: float, y: float):
        super(Vec2DInt, self).__init__(x=int(x), y=int(y))

    def copy(self):
        return Vec2DInt(x=self.x, y=self.y)

    def __str__(self):
        return f"Vec2DInt(x={self.x}, y={self.y})"

    def __repr__(self):
        return f"Vec2DInt(x={self.x}, y={self.y})"


################## Helper Functions ##############################################
def replace_color(cv2_image, color_to_replace, replace_color):
    """
    Replace a specified color inside an image.
    :param cv2_image: np.array(h,w,3).
    :param color_to_replace: tuple (b,g,r)
    :param replace_color: tuple (b,g,r)
    :return: image with replaced color.
    """
    cv2_image = cv2_image.copy()
    r, g, b = cv2_image[:, :, 0], cv2_image[:, :, 1], cv2_image[:, :, 2]
    mask = np.logical_and(r == color_to_replace[0], np.logical_and(g == color_to_replace[1], b == color_to_replace[2]))
    cv2_image[mask, :] = replace_color
    return cv2_image


def get_z_rotation(points):
    """
    Computes the angle by how much the points must be rotated to form a vertical line.
    :param points: list of two Vec2D objects.
    :return: degree of the angle.
    """
    v_rot = points[1] - points[0]  # Get vector of current line between both points
    # if v_rot[0] == 0:             # check if 0 degree or 180 degree Not needed?
    #     if v_rot[1] < 0: return 180
    #     return 0
    point_two = Vec2D(x=points[0].x, y=points[0].y + v_rot.norm())  # Get vertical vector with magnitude of rotated vector

    v_vert = point_two - points[0]
    v_hyp = v_rot - v_vert  # Get hypotenuse vector
    vorzeichen = +1  # Handle both sides of the 180 degree directions
    if v_rot.x > 0:
        vorzeichen = -1
    z_rotation_rad = 2 * np.arcsin(v_hyp.norm() / 2 / v_vert.norm())
    z_rotation_deg = z_rotation_rad * 180 / np.pi
    degree = vorzeichen * z_rotation_deg
    return degree


def rotate_points(points, rot_mat):
    """
    Rotate points according to rotation matrix.
    :param points: list of two Vec2D objects.
    :param rot_mat: cv2 rotation matrix.
    :return: rotated points.
    """
    points[0] = Vec2D(*(rot_mat @ np.array([points[0].x, points[0].y, 1])))
    points[1] = Vec2D(*(rot_mat @ np.array([points[1].x, points[1].y, 1])))
    return points


def draw_circles(cv2_image, points):
    """
    Draw given points onto the image.
    :param cv2_image: np.array(h,w,3).
    :param points: list of two Vec2D objects.
    :return: image with points drawn onto it.
    """
    blue = (255, 0, 0)
    if not isinstance(points[0], Vec2DInt): points[0] = points[0].convert_to_Vec2dInt()
    if not isinstance(points[1], Vec2DInt): points[1] = points[1].convert_to_Vec2dInt()
    output = cv2.circle(cv2_image, (points[0].x, points[0].y), radius=2, color=blue, thickness=3)
    return cv2.circle(output, (points[1].x, points[1].y), radius=2, color=blue, thickness=3)


def visualize_image(cv2_image):
    """
    Helper fucntion to visualize the given image inside a jupyter notebook.
    :param cv2_image: np.array(h,w,3)
    :return: PIL.Image object
    """
    im_rgb = cv2.cvtColor(cv2_image.astype(np.uint8), cv2.COLOR_BGR2RGB)
    return Image.fromarray(im_rgb)


def convertRelPointsToAbsPoints(points, height, width):
    """
    Helper function to convert points in [0,1] to [0,width] and [0,height].
    :param points: list of two Vec2D objects.
    :param height: height of the image in which the points lie.
    :param width: width of the image in which the points lie.
    :return: absolute points.
    """
    shape = Vec2D(x=width, y=height)
    return [points[0] * shape, points[1] * shape]


def draw_mask_on_image(image, mask, color=(0, 255, 255)):
    """
    Draws a mask onto the given image.
    :param image: np.array(h,w,3).
    :param mask: boolean array with the same size as the image.
    :param color: color of the mask.
    :return: image with drawn mask.
    """
    result = image.copy()
    kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (3, 3))
    dilated = cv2.morphologyEx(mask.astype(np.uint8), cv2.MORPH_DILATE, kernel)
    outline = dilated > mask
    result[mask == 1] = (result[mask == 1] * 0.9 + np.array(color) * 0.1).astype(np.uint8)
    result[outline] = (color[0] / 255 * 100, color[1] / 255 * 100, color[2] / 255 * 100)
    return result


def get_font_scale(image_shape):
    """
    Comppute how big the font should be according to the image shape.
    :param image_shape: (height, width, channel).
    :return: size of the font.
    """
    h, w, = image_shape[0:2]
    return w / 500


def write_text_on_image(cv2_image, text, text_layer=0, color=0):
    """
    Write text onto given image.
    :param cv2_image: np.arrays(h,w,3).
    :param text: string that should be drawn onto the image.
    :param text_layer: if previously text was written on the image the new text must be written below it.
    :param color: color of the text
    :return: image with text written onto it.
    """
    font_scale = get_font_scale(cv2_image.shape)
    text_size = cv2.getTextSize(text, cv2.FONT_HERSHEY_SIMPLEX, fontScale=font_scale, thickness=1)
    w, h = text_size[0][:2]
    if color == 2:
        color = (238, 178, 0)
    elif color == 1:
        color = (128, 128, 128)
    else:
        color = (255, 255, 255)
    result = cv2.rectangle(cv2_image, (0, h * text_layer + (1 * text_layer)), (w, h * (text_layer + 1) + (1 * text_layer)), (0, 0, 0), -1)
    result = cv2.putText(result, text, org=(0, h * (text_layer + 1) + (1 * text_layer)), fontScale=get_font_scale(cv2_image.shape), color=color, thickness=1,
                         fontFace=cv2.FONT_HERSHEY_SIMPLEX)
    return result


def concat_images(cv2_image_list, text=None, colors=None, boundary=None, mode=ConcatMode.VERT):
    """
    Concatenates all images given inside cv2_image_list either vertical or horizontal and placing boundaries between each image pair.
    :param cv2_image_list: list of np.arrays(h,w,3).
    :param text: list of strings with same length as cv2_image_list. - optional
    :param colors: list of colors with same length as cv2_image_list. - optional
    :param boundary: if the boundary between the images must be customized. - optional
    :param mode: if ConcatMode.VERT then the next image will be appended to the bottom.
    if ConcatMode.HORIZONTAL then the next image will be appended to the right.
    :return: one np.array that contains all images from cv2_image_list.
    """
    """ConcatMode.VERT=0 and ConcatMode.HORIZONTAL=1 are chosen such that they can be used for indexing the axis."""
    concat = []
    shape = cv2_image_list[0].shape
    if boundary is None:
        boundary_shape = np.array(shape)
        boundary_shape[mode] = min(int(boundary_shape[mode] * 0.01), 15)
        boundary = np.full(boundary_shape, fill_value=200)
    concat.append(boundary)
    for id, image in enumerate(cv2_image_list):
        image = image.copy()
        if mode == ConcatMode.VERT and shape[1] != image.shape[1]:
            factor = shape[1] / image.shape[1]
            image = cv2.resize(image.astype(np.uint8), (shape[1], int(image.shape[0] * factor)))
        elif mode == ConcatMode.HORIZONTAL and shape[0] != image.shape[0]:
            factor = shape[0] / image.shape[0]
            image = cv2.resize(image.astype(np.uint8), (int(image.shape[1] * factor), shape[0]))
        if colors is not None:
            color = colors[id]
        else:
            color = None
        if text is not None:
            image = write_text_on_image(image, text[id], text_layer=1, color=color)
        concat.append(image)
        concat.append(boundary)
    return np.concatenate(concat, axis=mode)


def concat_images_col(cv2_image_list, texts=None, colors=None, n_cols=1):
    """
    Concatenates n_cols images from cv2_image_list horizontally, adds a new row and concatenates the next n_cols images from cv2_image_list horizontally, until
    all images from cv2_image_list are concatenated.
    Example output when n_cols=3
    |--------------------|
    |image1|image2|image3|
    |image4|image5|image6|
    |--------------------|
    :param cv2_image_list: list of np.arrays(h,w,3).
    :param texts: list of strings with same length as cv2_image_list. Strings are drawn onto the corresponding image. - optional
    :param colors: list of colors with same length as cv2_image_list. Adjusts the color of the text that is drawn onto the image. - optional
    :param n_cols: integer specifying number of columns.
    :return: np.array representing the images concatenated column and row wise.
    """
    vert_concat = []
    for i in range(math.ceil(len(cv2_image_list) / n_cols)):
        bunch = [image.astype(np.uint8) for image in cv2_image_list[i * n_cols:(i + 1) * n_cols]]
        if texts is None:
            text_bunch = None
        else:
            text_bunch = [text for text in texts[i * n_cols:(i + 1) * n_cols]]
        if colors is None:
            color_bunch = None
        else:
            color_bunch = [color for color in colors[i * n_cols:(i + 1) * n_cols]]
        for j in range(n_cols - len(bunch)):
            bunch.append(np.zeros_like(bunch[0]))
        vert_concat.append(concat_images(bunch, text_bunch, color_bunch, mode=ConcatMode.HORIZONTAL))
    return concat_images(vert_concat, mode=ConcatMode.VERT)


def calculate_angular_between_vecs(vec1, vec2):
    """
    Helper function to calculate the angular between two Vec2Ds
    :param vec1: Vec2D
    :param vec2: Vec2D
    :return: angular between the two vectors.
    """
    return np.arccos((vec1.x * vec2.x + vec1.y * vec2.y) / (vec1.norm() * vec2.norm())) * 180 / np.pi


def rescale_depth_images(depth_image):
    """
    Helper function to visualize the depth images. It scales them such that the lowest depth value equals 0 and the highest depth value equals 255.
    :param depth_image: np.array(h,w,x).
    :return: rescaled depth image.
    """
    """Helper function to rescale the values inside of depth images so they can be visualized better."""
    depth_image = depth_image.copy()
    depth_image -= depth_image.min()
    depth_image = depth_image / depth_image.max() * 255
    return depth_image


def concat_horizontal(cv2_image_list, text=None):
    "Depricated"
    return concat_images(cv2_image_list, text=text, mode=ConcatMode.HORIZONTAL)


def concat_vert(cv2_image_list):
    "Depricated"
    return concat_images(cv2_image_list, mode=ConcatMode.VERT)