import cv2
import numpy as np
from Handler import utils
from Handler.utils import Vec2D, Vec2DInt


class RotationHandler:
    """This RotationHandler is used to implement the rotation and cutout of the images in order to be able to feed the cutout_image to
    the neural network in a later stage. It stores also all intermediate results which can then be accessed from outside.
    Is used by the FileHandler.
    Images are always cv2_images with either dtype=np.unit8 or dtype=np.float32,
    points is always a list containing two Vec2D, outlines are always a list of two Vec2DInt
    The RotationHandler needs to be initialized with the init method. After that arbitrary images can be rotated and cut the same way as the first one as long as
    they have the same shape.
    """

    def __init__(self, cv2_image: np.array, points: list):
        """
        Rotates and cuts the given image and stores the intermediate steps. After that the rotate_and_cut function can be used to rotate and cut another image
         the same way as the first image.
        :param cv2_image: numpy array with either dtype=np.unit8 or dtype=np.float32,
        :param points: list containing two Vec2D
        """
        square_image, square_points, square_outline = self.__create_square_images(cv2_image, points)
        rotated_image, rotated_points, angle = self.__rotate_image(square_image, square_points)
        cutout_image, cutout_points, cutout_outline = self.__cutout_rectangle(rotated_image, rotated_points)

        self.input_images = [cv2_image]
        self.input_points = points

        self.square_images = [square_image]
        self.square_points = square_points
        self.square_outlines = square_outline

        self.rotated_images = [rotated_image]
        self.rotated_points = rotated_points
        self.angle = angle

        self.cutout_images = [cutout_image]
        self.cutout_points = cutout_points
        self.cutout_outlines = cutout_outline

    def __getitem__(self, item_number):
        """Make intermediate results accessible to the outside."""
        return self.input_images[item_number], self.square_images[item_number], self.rotated_images[item_number], self.cutout_images[item_number]

    def rotate_and_cut(self, cv2_image: np.array):
        """
        Rotates and cuts the given cv2_image the same way as during the initialization. Stores the intermediate results and the end result.
        :param cv2_image: np.array(h,w,3) must have the same shape as the cv2_image with which the handler was initialized.
        """
        assert(cv2_image.shape[:2] == self.input_images[0].shape[:2])

        square_image, _, _ = self.__create_square_images(cv2_image, self.input_points)
        rotated_image, _, _ = self.__rotate_image(square_image, angle=self.angle)
        cutout_image, _, _ = self.__cutout_rectangle(rotated_image, self.rotated_points)

        self.input_images.append(cv2_image)
        self.square_images.append(square_image)
        self.rotated_images.append(rotated_image)
        self.cutout_images.append(cutout_image)

    def reverse_rotate_and_cut(self, cutout_cv2_image: np.array):
        """
        Gets an image which was cutout and rotated and reverses the cutout and rotation.
        :param cutout_cv2_image: np.array(h,w,3). Must have the same shape as self.cutout_images[0].
        :return: np.array(newH, newW, 3) with cutout_cv2_image having the same position as before the cutout and rotation. Missing values are filled with zeros.
        """
        assert(cutout_cv2_image.shape[:2] == self.cutout_images[0].shape[:2])

        rotated_image = np.zeros_like(self.rotated_images[0])
        left_top, right_bot = self.cutout_outlines
        rotated_image[left_top.y:right_bot.y, left_top.x:right_bot.x] = cutout_cv2_image
        h, w = rotated_image.shape[:2]
        rot_mat = cv2.getRotationMatrix2D(center=(h / 2, w / 2), angle=-self.angle, scale=1)
        square_image = cv2.warpAffine(rotated_image, rot_mat, (w, h))
        left_top, right_bot = self.square_outlines
        org_image = square_image[left_top.y:right_bot.y, left_top.x:right_bot.x]
        return org_image

    def __cutout_rectangle(self, cv2_image, points):
        """
        Helper function to cut out a region around the given wind turbine points such that the entire wind turbine is inside the cutout image.
        :param cv2_image: np.array(h,w,3).
        :param points: list of two Vec2D objects, representing the wind turbine points.
        :return: Cutout image np.array(newH,newW,3) and points at which the cutout image was extracted inside the cv2_iamge.
        """
        # Scale offset according to imagesize
        left_top, right_bot = self.__get_region(points)
        cutout_left_top = Vec2DInt(y=max(0, left_top.y), x=max(0, left_top.x))
        cutout_right_bot = Vec2DInt(y=min(cv2_image.shape[0], right_bot.y), x=min(cv2_image.shape[1], right_bot.x))
        cut_image = cv2_image[cutout_left_top.y:cutout_right_bot.y, cutout_left_top.x:cutout_right_bot.x]

        if cut_image.shape[0] <= 0 or cut_image.shape[1] <= 0:
            raise Exception("Nothing to cutout: Windturbine probably not visible in the image.")
        return cut_image, None, [cutout_left_top, cutout_right_bot]

    def __get_region(self, points) -> (Vec2DInt, Vec2DInt):
        """Helper function for cutout_rectangle"""
        norm = (points[1] - points[0]).norm()
        offset = Vec2D(1 / 2 * norm, 1 / 8 * norm * 2)
        left_top = points[0] - offset
        right_bot = points[1] + offset
        return left_top.convert_to_Vec2dInt(), right_bot.convert_to_Vec2dInt()

    def __create_square_images(self, cv2_image, points):
        """Creates a container for the image so the rotation can take place without cutting information from the image away."""
        height, width, c = cv2_image.shape
        diagonal = int(np.linalg.norm(np.array([height, width])))
        left_top = Vec2DInt(x=((diagonal - width) // 2), y=((diagonal - height) // 2))
        right_bot = left_top + Vec2DInt(x=width, y=height)

        square_image = np.zeros((diagonal, diagonal, c), dtype=cv2_image.dtype)
        adjusted_points = [points[0] + left_top, points[1] + left_top]
        square_image[left_top.y:right_bot.y, left_top.x:right_bot.x] = cv2_image
        image_outline_in_square_image = [left_top, right_bot]

        return square_image, adjusted_points, image_outline_in_square_image

    def __rotate_image(self, square_image, square_points=None, angle=None):
        """Rotates the image according to the points orientation and returns the rotated image, points and the angle"""
        if square_points is not None:
            angle = utils.get_z_rotation(square_points)
        h, w = square_image.shape[:2]
        rot_mat = cv2.getRotationMatrix2D(center=(h/2, w/2), angle=angle, scale=1)

        rotated_points = None
        if square_points is not None:
            rotated_points = utils.rotate_points(square_points, rot_mat)
        rotated_image = cv2.warpAffine(square_image, rot_mat, (w,h))

        return rotated_image, rotated_points, angle