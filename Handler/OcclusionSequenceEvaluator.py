import math

import numpy as np
from matplotlib import pyplot as plt

from Handler import utils
from Handler.utils import ConcatMode
import pandas as pd


class OcclusionSequenceEvaluator:
    """
    Used by the SequenceAnalysisHandler to store the metrics for each Scene regarding the occlusion results.
    After all metrics of the entire Sequence have been stored in this class, the class can be used to compute different statistics.
    """

    def __init__(self):
        self.image_list = []
        self.fprs = []
        self.fnrs = []

    def add_data(self, image, fpr, fnr):
        self.image_list.append(image)
        self.fprs.append(fpr)
        self.fnrs.append(fnr)

    def visualize_images(self, n_cols):
        vert_concat = []
        for i in range(math.ceil(len(self.image_list) / n_cols)):
            bunch = [image.astype(np.uint8) for image in self.image_list[i * n_cols:(i + 1) * n_cols]]
            for j in range(n_cols - len(bunch)):
                bunch.append(np.zeros_like(bunch[0]))
            vert_concat.append(utils.concat_images(bunch, mode=ConcatMode.HORIZONTAL))

        return utils.concat_images(vert_concat, mode=ConcatMode.VERT)

    def plot_fprs_and_fnrs(self, title=""):
        x = np.arange(0, len(self.fprs))
        width = 0.4
        plt.figure(figsize=(15, 5))
        plt.bar(x - width / 2, np.array(self.fprs), width=width, color='r', label="FPR")
        plt.bar(x + width / 2, np.array(self.fnrs), width=width, color='orange', label="FNR")
        plt.xticks(x)
        plt.ylim(0, 1.02)
        plt.title(str(title))
        plt.xlabel("Scene Number")
        plt.ylabel("Rate")
        plt.xlim(0 - width, len(self.fprs) - 1 + width)
        plt.legend(loc='best')


class SfMAndSkyOcclusionSequenceEvaluator:
    """
    Uses three OcclusionSequenceEvaluator one for SfM, one for Sky and one for the combination of both to compute different statistics and compare them.
    """

    def __init__(self, sfm_evaluator, sky_evaluator, both_evaluator):
        self.sfm_evaluator = sfm_evaluator
        self.sky_evaluator = sky_evaluator
        self.both_evaluator = both_evaluator

    def output_means(self):
        print(f"sfm 30m fpr: {np.mean(self.sfm_evaluator.fprs)} fnr: {np.mean(self.sfm_evaluator.fnrs)}")
        print(f"sky fpr: {np.mean(self.sky_evaluator.fprs)} fnr: {np.mean(self.sky_evaluator.fnrs)}")
        print(f"both fpr: {np.mean(self.both_evaluator.fprs)} fnr: {np.mean(self.both_evaluator.fnrs)}")

    def plot_fpr_fnr_boxplot(self):
        df_sky = pd.DataFrame
        def plot_threshold_box_plot(self, title=""):
            chosen_thresholds = []
            for fprs, fnrs in zip(self.all_fprs, self.all_fnrs):
                chosen_thresholds.append(self.get_best_threshold(fprs, fnrs))
            plt.boxplot(chosen_thresholds, vert=False)
            plt.title(title)
            plt.xlabel("SfM Threshold in m")
            plt.show()

    def visualize_images(self, n_cols):
        result = None
        vert_concat = []
        for i in range(math.ceil(len(self.sfm_evaluator.image_list) / n_cols)):
            bunch_sfm = self.get_bunch(self.sfm_evaluator.image_list, i * n_cols, (i + 1) * n_cols)
            bunch_sky = self.get_bunch(self.sky_evaluator.image_list, i * n_cols, (i + 1) * n_cols)
            bunch_both = self.get_bunch(self.both_evaluator.image_list, i * n_cols, (i + 1) * n_cols)
            compare = []
            for images in zip(bunch_sfm, bunch_sky, bunch_both):
                compare.append(self.compare_comparison_iamges(*images))

            for j in range(n_cols - len(compare)):
                compare.append(np.zeros_like(compare[0]))
            vert_concat.append(utils.concat_images(compare, mode=ConcatMode.HORIZONTAL))

        return utils.concat_images(vert_concat, mode=ConcatMode.VERT)

    def get_bunch(self, image_list, id_start, id_end):
        bunch = [image.astype(np.uint8) for image in image_list[id_start:id_end]]
        return bunch

    def compare_comparison_iamges(self, sfm, sky, both):
        green = (0, 252, 124)
        red = (0, 0, 255)
        orange = (0, 252, 255)
        purple = (255, 0, 127)
        blue = (255, 0, 0)
        result = both.copy()
        result[np.logical_and(sfm==green, sky!=green)] = purple
        result[np.logical_and(sfm != green, sky == green)] = blue
        return result

    def plot_fprs_and_fnrs(self, title=""):
        x = np.arange(0, len(self.fprs))
        width = 0.4
        plt.figure(figsize=(15, 5))
        plt.bar(x - width / 2, np.array(self.fprs), width=width, color='r', label="FPR")
        plt.bar(x + width / 2, np.array(self.fnrs), width=width, color='orange', label="FNR")
        plt.xticks(x)
        plt.ylim(0, 1.02)
        plt.title(str(title))
        plt.xlabel("Image Number")
        plt.ylabel("Rate")
        plt.xlim(0 - width, len(self.fprs) - 1 + width)
        plt.legend(loc='best')