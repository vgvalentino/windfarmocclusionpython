import glob

import cv2
from tqdm import tqdm, trange

from Handler.AnalysisHandler import AnalysisHandler
from Handler.FineTunerSfMThreshold import FineTunerSfMThreshold
from Handler.OcclusionHandler import OcclusionHandler
from Handler.OcclusionSequenceEvaluator import OcclusionSequenceEvaluator, SfMAndSkyOcclusionSequenceEvaluator
from Handler import utils
from Handler.utils import ImageVariant, MDEThresholdMode


class SequenceAnalysisHandler:
    """
    The SequenceAnalysisHandler is a wrapper around an already initialized OcclusionHandler and uses the AnalysisHandler to analyze each Scene independently
    and then combines the evaluation results for the entire Sequence.
    In other words, does what the AnalysisHandler does but not only for single Scenes, but for the entire Sequence.
    """

    def __init__(self, occlusion_handler: OcclusionHandler, start_number=None, end_number=None):
        self.occlusion_handler = occlusion_handler
        if start_number is not None and end_number is not None:
            self.start_number = start_number
            self.end_number = end_number
        else:
            files = glob.glob(f"{occlusion_handler.file_handler.path_prefix}background_*")
            numbers = [int(file.split("_")[-1].split(occlusion_handler.file_handler.image_format)[0]) for file in files]
            self.start_number = min(numbers)
            self.end_number = max(numbers)

    """
    Analyze different occlusion results, by computing the metrics and comparison maps by iterating over the entire Sequence.
    Each function uses the OcclusionSequenceEvaluator to store the intermediate Scene metrics computed by the Analysis handler.
    In the end the evaluator is returned on which different evaluations can be performed.
    """
    def analyze_sky_segmentation_sequence(self, reverse=True):
        evaluator = OcclusionSequenceEvaluator()
        for i in trange(self.start_number, self.end_number + 1):
            analysis_handler = AnalysisHandler(self.occlusion_handler)
            vis, fpr, fnr = analysis_handler.analyze_sky_segmentation(i, reverse)
            evaluator.add_data(vis, fpr, fnr)
        return evaluator

    def analyze_sfm_occlusion_sequence(self, sfm_threshold=10):
        evaluator = OcclusionSequenceEvaluator()
        for i in trange(self.start_number, self.end_number + 1):
            analysis_handler = AnalysisHandler(self.occlusion_handler)
            vis, fpr, fnr = analysis_handler.analyze_sfm_occlusion(i, sfm_threshold)
            evaluator.add_data(vis, fpr, fnr)
        return evaluator

    def analyze_mde_occlusion_sequence(self, mde_threshold_mode: MDEThresholdMode, use_topography: bool, fastloading=False):
        evaluator = OcclusionSequenceEvaluator()
        for i in trange(self.start_number, self.end_number + 1):
            analysis_handler = AnalysisHandler(self.occlusion_handler)
            vis, fpr, fnr = analysis_handler.analyze_mde_occlusion(i, mde_threshold_mode, use_topography, fastloading)
            evaluator.add_data(vis, fpr, fnr)
        return evaluator

    def analyze_mde_sky_occlusion_sequence(self, mde_threshold_mode: MDEThresholdMode, use_topography: bool, fastloading=False):
        evaluator = OcclusionSequenceEvaluator()
        for i in trange(self.start_number, self.end_number + 1):
            analysis_handler = AnalysisHandler(self.occlusion_handler)
            vis, fpr, fnr = analysis_handler.analyze_mde_sky_occlusion(i, mde_threshold_mode, use_topography, fastloading)
            evaluator.add_data(vis, fpr, fnr)
        return evaluator

    def analyze_sfm_sky_cutout_occlusion_sequence(self, sfm_threshold=10):
        evaluator = OcclusionSequenceEvaluator()
        for i in trange(self.start_number, self.end_number + 1):
            analysis_handler = AnalysisHandler(self.occlusion_handler)
            vis, fpr, fnr = analysis_handler.analyze_sfm_sky_cutout_occlusion(i, sfm_threshold)
            evaluator.add_data(vis, fpr, fnr)
        return evaluator

    def analyze_sfm_mde_sky_occlusion_sequence(self, sfm_threshold=30, mde_threshold_mode=MDEThresholdMode.MIN_UF_MAX, use_topography=True):
        evaluator = OcclusionSequenceEvaluator()
        for i in trange(self.start_number, self.end_number + 1):
            analysis_handler = AnalysisHandler(self.occlusion_handler)
            vis, fpr, fnr = analysis_handler.analyze_sfm_mde_sky_occlusion(i, sfm_threshold, mde_threshold_mode, use_topography)
            evaluator.add_data(vis, fpr, fnr)
        return evaluator

    def analyze_sfm_mde_sky_cutout_occlusion_sequence(self, sfm_threshold=30, mde_threshold_mode=MDEThresholdMode.MIN_UF_MAX, use_topography=True):
        evaluator = OcclusionSequenceEvaluator()
        for i in trange(self.start_number, self.end_number + 1):
            analysis_handler = AnalysisHandler(self.occlusion_handler)
            vis, fpr, fnr = analysis_handler.analyze_sfm_mde_sky_cutout_occlusion(i, sfm_threshold, mde_threshold_mode, use_topography)
            evaluator.add_data(vis, fpr, fnr)
        return evaluator

    def analyze_sky_cutout_occlusion_sequence(self):
        evaluator = OcclusionSequenceEvaluator()
        for i in trange(self.start_number, self.end_number + 1):
            analysis_handler = AnalysisHandler(self.occlusion_handler)
            vis, fpr, fnr = analysis_handler.analyze_sky_cutout_occlusion(i)
            evaluator.add_data(vis, fpr, fnr)
        return evaluator

    def analyze_sfm_sky_occlusion_sequence(self, sfm_threshold=10):
        evaluator_sfm = OcclusionSequenceEvaluator()
        evaluator_sky = OcclusionSequenceEvaluator()
        evaluator_combined = OcclusionSequenceEvaluator()
        for i in trange(self.start_number, self.end_number + 1):
            analysis_handler = AnalysisHandler(self.occlusion_handler)
            (vis1, fpr1, fnr1), (vis2, fpr2, fnr2), (vis3, fpr3, fnr3) = analysis_handler.analyze_sfm_sky_occlusion(i, sfm_threshold)
            evaluator_sfm.add_data(vis1, fpr1, fnr1)
            evaluator_sky.add_data(vis2, fpr2, fnr2)
            evaluator_combined.add_data(vis3, fpr3, fnr3)
        sfm_sky_evaluator = SfMAndSkyOcclusionSequenceEvaluator(evaluator_sfm, evaluator_sky, evaluator_combined)
        return sfm_sky_evaluator

    """
    Finetune SfM threshold without and in combination with Sky Segmentation for the entire Sequence.
    """
    def fine_tune_sfm_threshold_sequence(self):
        finetuning_dataholders = []
        for i in tqdm(range(self.start_number, self.end_number + 1), desc="SeqenceAnalysisHandler: Finetuning sfm Sequence:", position=1):
            analysis_handler = AnalysisHandler(self.occlusion_handler)
            dataholder = analysis_handler.finetune_sfm_threshold(i)
            dataholder.images = None  # Otherwise we get a Out of memory exception for a big sequence
            finetuning_dataholders.append(dataholder)
        return FineTunerSfMThreshold(finetuning_dataholders)

    def fine_tune_sfm_and_sky_threshold_sequence(self, fpr_weight, fnr_weight):
        finetuning_dataholders = []
        for i in tqdm(range(self.start_number, self.end_number + 1), desc="SeqenceAnalysisHandler: Finetuning sfm Sequence:", position=1):
            analysis_handler = AnalysisHandler(self.occlusion_handler)
            dataholder = analysis_handler.finetune_sfm_and_sky_threshold(i)
            dataholder.images = None  # Otherwise we get an Out of memory exception for a big sequence
            finetuning_dataholders.append(dataholder)
        return FineTunerSfMThreshold(finetuning_dataholders, fpr_weight=fpr_weight, fnr_weight=fnr_weight)

    """
    Visualize Occlusions. Iterate through the entire Sequence and concatenate the visualizations results and return it.
    """
    def visualize_mde_sky_occlusion_sequence(self, mde_threshold_mode: MDEThresholdMode, use_topography: bool, fastloading=False):
        concat = []
        for i in trange(self.start_number, self.end_number + 1):
            analysis_handler = AnalysisHandler(self.occlusion_handler)
            occluded_image = analysis_handler.visualize_mde_sky_occlusion(i, mde_threshold_mode, use_topography, fastloading)[0]
            concat.append(occluded_image)
        return concat

    def visualize_occluded_sfm_and_sky_images(self, sfm_threshold=10, n_cols=1):
        concat = []
        for i in range(self.start_number, self.end_number + 1):
            self.occlusion_handler.load_scene(i)
            concat.append(self.occlusion_handler.visualize_sfm_sky_occlusion(sfm_threshold))

        return utils.concat_images_col(concat, n_cols=n_cols)

    def visualize_occluded_sfm_images(self, sfm_threshold=10, n_cols=1):
        concat = []
        for i in range(self.start_number, self.end_number + 1):
            self.occlusion_handler.load_scene(i)
            concat.append(self.occlusion_handler.visualize_sfm_occlusion(sfm_threshold))

        return utils.concat_images_col(concat, n_cols=n_cols)

    def visualize_occluded_sky_images(self, n_cols=1):
        concat = []
        for i in range(self.start_number, self.end_number + 1):
            self.occlusion_handler.load_scene(i)
            concat.append(self.occlusion_handler.visualize_sky_occlusion())

        return utils.concat_images_col(concat, n_cols=n_cols)

    def visualize_dataset(self, screen=True, n_cols=1):
        concat = []
        for i in range(self.start_number, self.end_number + 1):
            self.occlusion_handler.load_scene(i)
            if screen: image = self.occlusion_handler.file_handler.get_screen(ImageVariant.FULL)
            else: image = self.occlusion_handler.file_handler.get_background(ImageVariant.FULL)
            concat.append(image)
        return utils.concat_images_col(concat, n_cols=n_cols)

    def visualize_dataset_with_ground(self, screen=True, n_cols=1):
        concat = []
        for i in range(self.start_number, self.end_number + 1):
            self.occlusion_handler.load_scene(i)
            if screen: image = self.occlusion_handler.file_handler.get_screen(ImageVariant.FULL)
            else: image = self.occlusion_handler.file_handler.get_background(ImageVariant.FULL)
            concat.append(image)
            concat.append(self.occlusion_handler.file_handler.get_groundtruth(ImageVariant.FULL))
        return utils.concat_images_col(concat, n_cols=2 * n_cols)

    def visualize_dataset_with_sfm_depth(self, n_cols=1):
        concat = []
        for i in range(self.start_number, self.end_number + 1):
            self.occlusion_handler.load_scene(i)
            image = self.occlusion_handler.file_handler.background_org
            concat.append(image)
            concat.append(utils.rescale_depth_images(self.occlusion_handler.file_handler.sfm_depth))
        return utils.concat_images_col(concat, n_cols=2 * n_cols)

    def visualize_long_distance_occluded_images(self, n_cols=1, mde_threshold_mode=MDEThresholdMode.UF, use_topography=True, fastloading=True):
        concat = []
        for i in range(self.start_number, self.end_number + 1):
            self.occlusion_handler.load_scene(i)
            concat.append(self.occlusion_handler.visualize_occlusion_long_distance(mde_threshold_mode, use_topography, fastloading))

        return utils.concat_images_col(concat, n_cols=n_cols)

    def visualize_global_occluded_images_org_size(self, n_cols=1, sfm_thresh=30, mde_thresh_mode=MDEThresholdMode.UF, use_topography=True, fastloading=True):
        concat = []
        for i in range(self.start_number, self.end_number + 1):
            self.occlusion_handler.load_scene(i)
            concat.append(self.occlusion_handler.visualize_combined_occlusion_full(sfm_thresh, mde_thresh_mode, use_topography, fastloading))

        return utils.concat_images_col(concat, n_cols=n_cols)

    def visualize_global_occluded_images_cutout_size(self, n_cols=1, sfm_thresh=30, mde_thresh_mode=MDEThresholdMode.UF, use_topography=True, fastloading=True):
        concat = []
        for i in range(self.start_number, self.end_number + 1):
            self.occlusion_handler.load_scene(i)
            concat.append(self.occlusion_handler.visualize_global_occlusion_cutout(sfm_thresh, mde_thresh_mode, use_topography, fastloading))

        return utils.concat_images_col(concat, n_cols=n_cols)

    """
    Does not return the entire Sequence, but writes it to the specified path because of memory issues.
    """
    def visualize_dataset_sequence(self, fastloading=True):
        concat = []
        for i in trange(self.start_number, self.end_number + 1):
            analysis_handler = AnalysisHandler(self.occlusion_handler)
            image = analysis_handler.visualize_dataset(i, fastloading=fastloading)
            concat.append(image)

        for scene_id, image in enumerate(concat):
            path = self.occlusion_handler.file_handler.path_prefix
            cv2.imwrite(f"{path}visualization_{self.start_number+scene_id}.jpg", image)
        return None
