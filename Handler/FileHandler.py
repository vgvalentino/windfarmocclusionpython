from keras.models import load_model

from DenseDepth.layers import BilinearUpSampling2D

import cv2
import numpy as np
from Handler import utils
from Handler.RotationHandler import RotationHandler
from Handler.utils import Vec2D, ImageVariant


class FileHandler:
    """
    Handles everything related to the given files.
    OcclusionHandler uses the FileHandler to request all needed files.
    """

    def __init__(self, path_prefix, image_format=".png", model="./DenseDepth/nyu.h5"):
        self.path_prefix = path_prefix
        self.image_format = image_format

        if type(model) == str:
            self.model = load_model(model, custom_objects={'BilinearUpSampling2D': BilinearUpSampling2D, 'depth_loss_function': None}, compile=False)
            print('\nModel loaded ({0}).'.format(self.model))
        else:
            self.model = model

        self.scene_number = None
        self.rotation_handler: RotationHandler = None
        # self.mde_handler: MDEHandler = None
        # self.sfm_sky_handler = None

        self.sfm_depth, self.background_org, self.groundtruth_org = None, None, None
        self.points, self.windturbine_distance = None, None

    def load_scene(self, scene_number: int):
        """
        Loads the files from the Scene given by scene_number and the path_prefix.
        :param scene_number: integer, number of scene to load.
        """
        self.scene_number = scene_number
        screen_image, backround_image, topo_depth, groundtruth, self.sfm_depth, self.background_org, self.groundtruth_org, topo_fix = \
            self.__load_images(self.path_prefix, self.image_format, scene_number)
        self.points, self.windturbine_distance = self.__load_points(self.path_prefix, scene_number, screen_image)

        # rotate and cutout everything with a new RotationHandler
        self.rotation_handler = RotationHandler(screen_image, self.points)
        self.rotation_handler.rotate_and_cut(backround_image)
        self.rotation_handler.rotate_and_cut(topo_depth)
        self.rotation_handler.rotate_and_cut(groundtruth)
        self.rotation_handler.rotate_and_cut(topo_fix)

    def get_screen(self, variant: ImageVariant):
        """Helper function to access the screen image variants from the rotation handler."""
        return self.rotation_handler[0][variant]

    def get_background(self, variant: ImageVariant):
        """Helper function to access the background image variants from the rotation handler."""
        return self.rotation_handler[1][variant]

    def get_topo(self, variant: ImageVariant):
        """Helper function to access the topographic depth data variants from the rotation handler."""
        return self.rotation_handler[2][variant]

    def get_groundtruth(self, variant: ImageVariant):
        """Helper function to access the ground truth variants from the rotation handler."""
        return self.rotation_handler[3][variant]

    def cut_away(self, cv2_image):
        """Helper function to cut away the left and right side of the image."""
        cut_away = self.need_to_cut_away
        cv2_image_adjusted = cv2_image[:, cut_away:cv2_image.shape[1] - cut_away]
        return cv2_image_adjusted

    def __adjust_background_and_screen_image_size(self, screen_image, background_image, groundtruth_image, topo_depth):
        """The camera image (background image) has a different aspect ration than the screen image. Therefore, it is adjusted to fit the screen image."""
        sH, sW, _ = screen_image.shape
        bH, bW, _ = background_image.shape
        screenRatio = sW / sH
        self.need_to_cut_away = int((bW - bH * screenRatio) // 2)

        background_adjusted = self.cut_away(background_image)
        groundtruth_adjusted = self.cut_away(groundtruth_image)

        # background_adjusted = background_image[:, need_to_cut_away:bW - need_to_cut_away]
        # groundtruth_adjusted = groundtruth_image[:, need_to_cut_away:bW - need_to_cut_away]

        newHeight, newWidth = background_adjusted.shape[:2]
        screen_adjusted = cv2.resize(screen_image, (newWidth, newHeight))
        topo_adjusted = cv2.resize(topo_depth, (newWidth, newHeight))

        assert background_adjusted.shape == screen_adjusted.shape, f"FileHandler adjusted background to screen image was not successful. {background_image.shape}, {screen_image.shape}"
        return screen_adjusted, background_adjusted, groundtruth_adjusted, topo_adjusted

    def __load_images(self, path_prefix, image_format=".png", image_number=0):
        """Loads all needed images and makes sure that they all have the same shape"""
        # Load Scene Images
        screen_image = cv2.imread(f"{path_prefix}screen_{image_number}{image_format}")
        background_image = cv2.imread(f"{path_prefix}background_{image_number}{image_format}")  # because of problem with saving images
        background_image = cv2.rotate(background_image, cv2.ROTATE_90_COUNTERCLOCKWISE)
        groundtruth_image = cv2.imread(f"{path_prefix}groundtruth_{image_number}{image_format}")
        groundtruth_image = cv2.rotate(groundtruth_image, cv2.ROTATE_90_COUNTERCLOCKWISE)
        topo_fix = cv2.imread(f"{path_prefix}groundtruth_{image_number}{image_format}")
        topo_fix = cv2.rotate(topo_fix, cv2.ROTATE_90_COUNTERCLOCKWISE)
        if screen_image is None or background_image is None or groundtruth_image is None:
            raise FileNotFoundError("Screen or background or groundtruth image do not exist")
        h, w = screen_image.shape[:2]

        # Load Depth Images from .txt files containing either float16 or float32 depth values and adjusting them to the Scene Images
        topo_depth = np.flip(np.fromfile(f"{path_prefix}topo_{image_number}.txt", dtype=np.float16), axis=0).astype(np.float32)  # depth values in 1/distance
        topo_depth = topo_depth.reshape((h, w, -1))
        topo_depth = cv2.flip(topo_depth, 1)
        topo_depth = np.stack([topo_depth, topo_depth, topo_depth], axis=2)

        sfm_depth = np.fromfile(f"{path_prefix}sfm_{image_number}.txt", dtype=np.float32)  # depht values in meters
        sfm_depth = sfm_depth.reshape((120, 160))
        sfm_depth = cv2.flip(sfm_depth, 1)
        sfm_depth = cv2.rotate(sfm_depth, cv2.ROTATE_90_CLOCKWISE)
        sfm_depth = np.stack([sfm_depth, sfm_depth, sfm_depth], axis=2)

        screen_image, background_image_adjusted, groundtruth_image_adjusted, topo_depth = self.__adjust_background_and_screen_image_size(screen_image,
                                                                                                            background_image, groundtruth_image, topo_depth)
        topo_fix = self.cut_away(topo_fix)

        assert screen_image.shape == topo_depth.shape and screen_image.shape == background_image_adjusted.shape and \
               screen_image.shape == groundtruth_image_adjusted.shape, f"Load Images: {screen_image.shape} != {background_image_adjusted.shape} " \
                                                                       f"!= {groundtruth_image_adjusted.shape} != {topo_depth.shape}"
        assert background_image.shape == groundtruth_image.shape, \
            f"Load images org background and groundtruth: {background_image.shape} != {groundtruth_image.shape}"
        return screen_image, background_image_adjusted, topo_depth, groundtruth_image_adjusted, sfm_depth, background_image, groundtruth_image, topo_fix

    @staticmethod
    def __load_points(path_prefix, image_number, screen_image):  # TODO until now it does not make sure that the points are in the image
        """Loads points from pointfile and returns points scaled according to the screen_image. Also extracts the wind turbine distance."""
        with open(f"{path_prefix}points_{image_number}.txt", "r") as file:
            line = file.readline().replace("(", "", -1).replace(")", "", -1)
            values = line.split(",")
            rel_points = [Vec2D(x=float(values[3]), y=float(values[4])), Vec2D(x=float(values[0]), y=float(values[1]))]
            windturbine_distance = min(float(values[2]), float(values[5]))
            # Adjust points to different coordinate system
            rel_points[0].y = 1 - rel_points[0].y
            rel_points[1].y = 1 - rel_points[1].y
            points = utils.convertRelPointsToAbsPoints(rel_points, *screen_image.shape[0:2])
        return points, windturbine_distance