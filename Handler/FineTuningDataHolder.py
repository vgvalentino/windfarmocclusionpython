from Handler import utils


class FineTuningDataHolder:
    """
    Helper class to store important data until all needed Scenes are analyzed.
    After that the FineTunerSfMThreshold class uses this class to access the computed metrics.
    """

    def __init__(self, fine_tuning_mode: utils.FineTuneMode):
        self.fine_tuning_mode = fine_tuning_mode
        self.images = []
        self.fprs = []
        self.fnrs = []
        self.topography = []
        self.modes = []
        self.thresholds = []

    def add_data_mde(self, image, fpr, fnr, topography: bool, mode: utils.MDEThresholdMode):
        if self.fine_tuning_mode != utils.FineTuneMode.MDE: raise Exception("Wrong data or wrong mode in FineTuning Class")
        self.images.append(image)
        self.fprs.append(fpr)
        self.fnrs.append(fnr)
        self.topography.append(topography)
        self.modes.append(mode)

    def add_data_sfm(self, image, fpr, fnr, threshold: float):
        if self.fine_tuning_mode != utils.FineTuneMode.SFM: raise Exception("Wrong data or wrong mode in FineTuning Class")
        self.images.append(image)
        self.fprs.append(fpr)
        self.fnrs.append(fnr)
        self.thresholds.append(threshold)