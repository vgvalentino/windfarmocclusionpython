
import time

import cv2
import numpy as np

from Handler import utils


class SkyHandler:
    """
    Handles everything related to the Sky-Segmentation-Algorithm.
    OcclusionHandler uses the SkyHandler to request the Sky occlusion map.
    """

    def __init__(self, background_image, points, topo_depth, windturbine_distance):
        self.background_image = background_image
        self.points = points
        self.topo_depth = topo_depth
        self.windturbine_distance = windturbine_distance

    def get_sky_occlusion_map(self):
        """
        Computes the sky occlusion map by using the sky segmentation. Used by OcclusionHandler.
        :return: Sky Occlusion Map being a boolean array with np.array(newH,newW,3; dtype=np.uint8).
        1=Occlude wind turbine, 0=Don't occlude wind turbine
        """
        topo_occl_map = self.get_topo_occlusion_map_for_sky_seg()
        sky_seg, sky_confidence_map, self.sky_mask, self.ground_mask = self.get_sky_segmentation(topo_occl_map)
        sky_occl = np.logical_not(sky_seg)
        newH, newW = self.background_image.shape[:2]
        sky_occlusion_map = cv2.resize(np.stack([sky_occl, sky_occl, sky_occl], axis=2).astype(np.uint8), (newW, newH))
        return sky_occlusion_map

    def get_sky_segmentation(self, topo_occlusion_map):
        """
        Uses topographic depth data to select a non-sky region and the wind turbine direction to select a sky region.
        After that the simplified k means algorithms is used to segment the downscaled camera image into sky and non-sky pixels.
        In addition to that the confidence of the algorithm is also computed. However, it is not used anywhere at this moment.
        :param topo_occlusion_map: np.array(h,w,3)
        :return sky segmentation np.array(160,120), confidence of the sky segmentation algorithm np.array(160,120),
        mask marking values used to compute the sky color distribution, and mask marking values used to compute the non-sky color distribution.
        """
        sky_mask = self.get_sky_mask(self.background_image, self.topo_depth.shape, self.points)
        ground_mask = topo_occlusion_map[:, :, 0]
        ground_mask = ground_mask > 1 / self.windturbine_distance
        downscaled_image = cv2.resize(self.background_image, (120, 160))

        segmentation, confidence = self.simplified_k_means(downscaled_image, self.background_image[sky_mask], self.background_image[ground_mask])

        sky_confidence_map = np.zeros_like(segmentation, dtype=float)
        sky_confidence_map[segmentation] = confidence
        return segmentation, sky_confidence_map, sky_mask, ground_mask

    def simplified_k_means(self, image, sky_init_pixels, ground_init_pixels):
        """
        Segmenting the given image, computing the sky and non-sky color distributions according to the given values in sky_init_pixels and ground_init_pixels.
        Then using the color distributions to devide image into sky and non-sky pixels.
        :param image: np.array(h,w,3)
        :param sky_init_pixels: values to compute sky color distribution.
        :param ground_init_pixels: values to compute non-sky color distribution.
        :return: segmentation np.array(h,w) 1=sky pixel 0=non-sky pixel. Confidence of classification np.array(h,w).
        """
        # start = time.time()
        color_channels = image.shape[2]
        data = image.reshape((-1, color_channels))

        sky_center = np.mean(sky_init_pixels.reshape((-1, color_channels)), axis=0)
        ground_center = np.mean(ground_init_pixels.reshape((-1, color_channels)), axis=0)

        sky_distance = np.linalg.norm(np.abs(data - sky_center), axis=1)
        ground_distance = np.linalg.norm(np.abs(data - ground_center), axis=1)

        labels = sky_distance < ground_distance
        segmentation = labels.reshape((image.shape[0], image.shape[1]))

        confidence = self.compute_sky_confidence(labels, sky_distance, ground_distance)
        # print(f"Time needed: {time.time() - start} with speed_up.")
        return segmentation, confidence

    def compute_sky_confidence(self, labels, sky_distance, ground_distance):
        """
        Helper function to compute the sky confidence classification for each pixel.
        :param labels: np.array(h*w).
        :param sky_distance: np.array(h*w).
        :param ground_distance: np.array(h*w).
        :return: Confidences np.array(h*w).
        """
        sky_conf = ground_distance[labels] / (sky_distance[labels] + ground_distance[labels])
        # normally sky_conf = 1 - sky_distance/sum, but it is the same as this
        return sky_conf

    def get_topo_occlusion_map_for_sky_seg(self):
        """
        Acquires the topo_occlusion_map and adjusts it to the bigger camera image, filling missing values with zeros.
        :return: topo_occlusion_map np.array(background_h, background_w, 3) with 1=foreground and 0=background pixel.
        """
        topo_occlusion_map = self.__get_topo_occlusion_map(self.topo_depth, self.windturbine_distance)
        adjsuted_to_bigger_background = np.zeros_like(self.background_image)
        width_back = self.background_image.shape[1]
        width_topo = topo_occlusion_map.shape[1]
        width_diff = width_back - width_topo
        adjsuted_to_bigger_background[:, width_diff//2:width_back-(width_diff//2)] = topo_occlusion_map

        return adjsuted_to_bigger_background

    @staticmethod
    def __get_topo_occlusion_map(topo_depth, windturbine_distance):
        topo_occlusion_map = topo_depth[:, :, 0] > 1 / windturbine_distance  # topo depth saved as 1/distance
        return np.stack([topo_occlusion_map, topo_occlusion_map, topo_occlusion_map], axis=2)

    @staticmethod
    def get_sky_mask(image, screen_image_shape, points):
        """
        Helper function to follow the vector of the two wind turbine points towards the image border
        and select there a sky region which is marked using a mask.
        :param image: background image np.array(h,w,3).
        :param screen_image_shape: screen image shape is different from background image shape.
        :param points: list of two Vec2D objects, representing the wind turbine points.
        :return: mask, specifying where the sky region is.
        """
        # adjust points to background image since points are for screen image and background image is wider.
        screen_image_shape = utils.Vec2D(y=screen_image_shape[0], x=screen_image_shape[1])
        image_shape = utils.Vec2D(y=image.shape[0], x=image.shape[1])
        diff = (image_shape - screen_image_shape) / 2

        p_zero = (points[0] + diff)
        p_one = (points[1] + diff)

        mask = np.zeros((image.shape[0], image.shape[1]), dtype=bool)
        v_rot = (p_zero - p_one).normalized()
        left_top = utils.Vec2DInt(x=0, y=0)
        right_top = utils.Vec2DInt(x=image.shape[1], y=0)
        left_bot = utils.Vec2DInt(x=0, y=image.shape[0])
        right_bot = utils.Vec2DInt(x=image.shape[1], y=image.shape[0])
        v_left_top = (left_top - p_zero).normalized()
        v_right_top = (right_top - p_zero).normalized()
        v_left_bot = (left_bot - p_zero).normalized()
        v_right_bot = (right_bot - p_zero).normalized()

        if v_rot.x >= v_right_top.x and v_rot.x >= v_right_bot.x:  # will hit right boundary
            y = None
            x = image.shape[1]
        elif v_rot.x <= v_left_top.x and v_rot.x <= v_left_bot.x:  # will hit left boundary
            y = None
            x = 0
        elif v_rot.y >= v_left_top.y and v_rot.y >= v_right_top.y:  # will hit bot boundary
            y = image.shape[0]
            x = None
        elif v_rot.y <= v_left_bot.y and v_rot.y <= v_right_bot.y:  # will hit top boundary
            y = 0
            x = None

        if x is None:
            r = (y - p_zero.y) / v_rot.y
            x = int(r * v_rot.x + p_zero.x)
        if y is None:
            r = (x - p_zero.x) / v_rot.x
            y = int(r * v_rot.y + p_zero.y)

        mask[max(y - 50, 0):min(y + 50, image.shape[0]), max(x - 50, 0):min(x + 50, image.shape[1])] = 1
        return mask