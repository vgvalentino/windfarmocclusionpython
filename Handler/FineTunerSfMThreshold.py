from typing import List

from Handler.FineTuningDataHolder import FineTuningDataHolder
from Handler.utils import ConcatMode
from matplotlib import pyplot as plt
import numpy as np
from Handler import utils
import cv2
import math


class FineTunerSfMThreshold:
    """
    Uses the stored metrics in FineTuningDataHolders. To compute different statistics over an entire Sequence in order to fine tune the SfM threshold.
    """

    def __init__(self, finetuner_dataholders: List[FineTuningDataHolder], fpr_weight=0.6, fnr_weight=0.4):
        self.fpr_weight = fpr_weight
        self.fnr_weight = fnr_weight
        self.all_images = []
        self.all_fprs = np.zeros((len(finetuner_dataholders), len(finetuner_dataholders[0].fprs)))
        self.all_fnrs = np.zeros_like(self.all_fprs)
        for id, dataholder in enumerate(finetuner_dataholders):
            if dataholder.fine_tuning_mode != utils.FineTuneMode.SFM: raise Exception("Wrong data or wrong mode in FineTuning Class")
            self.all_images.append(dataholder.images)
            self.all_fprs[id] = np.array(dataholder.fprs)
            self.all_fnrs[id] = np.array(dataholder.fnrs)
        self.median_fprs = np.median(self.all_fprs, axis=0)
        self.median_fnrs = np.median(self.all_fnrs, axis=0)

        self.thresholds = np.array(finetuner_dataholders[0].thresholds)  # all threshold lists are the same otherwise the comparison would not make sense

    def visualize_images(self, only_relevant=False, path=""):
        """Do not return visualization because the needed numpy array would be too large. Instead save each Scene visualization as a file."""
        threshold_texts = [f"threshold={thresh:.2f}" for thresh in self.thresholds]
        if only_relevant:
            for scene_id, images in enumerate(self.all_images):
                prev_fpr, prev_fnr = -1, -1
                concat = []
                thresholds = []
                for threshold_id, image in enumerate(images):
                    if self.median_fprs[threshold_id] != prev_fpr or self.median_fnrs[threshold_id] != prev_fnr:  # image changed
                        prev_fpr, prev_fnr = self.median_fprs[threshold_id], self.median_fnrs[threshold_id]
                        concat.append(image)
                        thresholds.append(threshold_texts[threshold_id])
                cv2.imwrite(f"{path}{scene_id}.jpg", utils.concat_images(concat, text=thresholds, mode=ConcatMode.HORIZONTAL))
        else:
            for scene_id, images in enumerate(self.all_images):
                cv2.imwrite(f"{path}{scene_id}.jpg", utils.concat_images(images, threshold_texts, mode=ConcatMode.VERT))
            return

    def plot_fprs_against_fnrs_without_best_threshold(self, fpr, fnr, ax, label_fpr="", label_fnr="", title=""):
        ax.plot(self.thresholds, fpr, 'r', label=label_fpr)
        ax.plot(self.thresholds, fnr, 'orange', label=label_fnr)
        x_ticks = [0, 10, 20, 30, 40, 50, 60]
        ax.set_xticks(x_ticks)
        ax.set_title(str(title))
        ax.set_xlabel("SfM Threshold in m")
        ax.set_ylabel("Rate")
        ax.legend(loc='lower right', bbox_to_anchor=(1, 0.2))

    def plot_median_fprs_fnrs(self, title=""):
        fig, ax = plt.subplots()
        self.plot_fprs_against_fnrs(self.median_fprs, self.median_fnrs, ax, label_fpr="Median FPR", label_fnr="Median FNR", title=title)
        plt.show()

    def plot_all_fprs_fnrs(self, title="", cols_n=1):
        number_plots = len(self.all_fprs)
        rows_n = math.ceil(number_plots / cols_n)
        fig, axes = plt.subplots(rows_n, cols_n, figsize=(cols_n * 6.4, rows_n * 4.8))
        if cols_n == 1: axes = [axes]
        flat_axes = [item for sublist in axes for item in sublist]
        for scene_id, scene in enumerate(self.all_fprs):
            self.plot_fprs_against_fnrs(self.all_fprs[scene_id], self.all_fnrs[scene_id], flat_axes[scene_id], label_fpr="FPR", label_fnr="FNR",
                                        title=f"{title} Scene: {scene_id}")
        plt.show()

    def calculate_penalty(self, fprs, fnrs):
        return self.fpr_weight * fprs + self.fnr_weight * fnrs

    def get_best_threshold(self, fprs, fnrs):
        penalty = self.calculate_penalty(fprs, fnrs)
        indices = np.where(penalty == penalty.min())
        index = indices[0][-1]
        return self.thresholds[index]

    def plot_thresholds_for_all_scenes(self, title=""):
        for fprs, fnrs in zip(self.all_fprs, self.all_fnrs):
            best_thresh = self.get_best_threshold(fprs, fnrs)
            plt.axvline(x=best_thresh, color='b', ymin=0, ymax=1)
        if title is not None:
            plt.title(str(title))
        plt.xlabel("SfM Threshold in m")
        plt.ylabel("Nothing")
        plt.show()

    def plot_threshold_box_plot(self, title=""):
        chosen_thresholds = []
        for fprs, fnrs in zip(self.all_fprs, self.all_fnrs):
            chosen_thresholds.append(self.get_best_threshold(fprs, fnrs))
        plt.boxplot(chosen_thresholds, vert=False)
        plt.title(title)
        plt.xlabel("SfM Threshold in m")
        plt.show()

    def plot_fprs_against_fnrs(self, fpr, fnr, ax, label_fpr="", label_fnr="", title=""):
        """Depricated"""
        ax.plot(self.thresholds, fpr, 'r', label=label_fpr)
        ax.plot(self.thresholds, fnr, 'orange', label=label_fnr)
        x_ticks = [0, 10, 20, 30, 40, 50, 60]
        ax.set_xticks(x_ticks)
        ax.set_title(str(title))
        ax.set_xlabel("SfM Threshold in m")
        ax.set_ylabel("Rate")
        ax.legend(loc='lower right', bbox_to_anchor=(1, 0.2))
