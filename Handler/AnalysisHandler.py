from Handler.FineTuningDataHolder import FineTuningDataHolder
import numpy as np
from tqdm import tqdm

from Handler.OcclusionHandler import OcclusionHandler
from Handler import utils
from Handler.utils import ImageVariant, MDEThresholdMode, ConcatMode


class AnalysisHandler:
    """
    The AnalysisHandler is a wrapper around an already initialized OcclusionHandler. After the OcclusionHandler has loaded a scene the AnalysisHandler can be
    used to analyze the intermediate computation results.
    """
    def __init__(self, occlusion_handler: OcclusionHandler):
        self.occlusion_handler = occlusion_handler

    """
    Analyze different occlusion results, by computing the metrics and comparison maps.
    Each function first loads the Scene of the Sequence which shall be analyzed using the scene_number.
    Then the False Positive Rate and False Negative Rate are computed and visualized in a Comparison Map.
    """
    def analyze_sky_segmentation(self, scene_number: int, referse=True):
        self.occlusion_handler.load_scene(scene_number)
        sky_occl_map = self.occlusion_handler.get_sky_occlusion_map()
        if referse:
            vis, fpr, fnr = self.compare_with_groundtruth(np.logical_not(self.occlusion_handler.file_handler.groundtruth_org), np.logical_not(sky_occl_map))
        else:
            vis, fpr, fnr = self.compare_with_groundtruth(self.occlusion_handler.file_handler.groundtruth_org, sky_occl_map)
        return vis, fpr, fnr

    def analyze_sfm_occlusion(self, scene_number: int, sfm_threshold=10):
        self.occlusion_handler.load_scene(scene_number)
        sfm_occl_map = self.occlusion_handler.get_sfm_occlusion_map(sfm_threshold)
        vis, fpr, fnr = self.compare_with_groundtruth(self.occlusion_handler.file_handler.groundtruth_org, sfm_occl_map)
        return vis, fpr, fnr

    def analyze_mde_occlusion(self, scene_number: int, mde_threshold_mode: MDEThresholdMode, use_topography: bool, fastloading=False):
        self.occlusion_handler.load_scene(scene_number)
        self.occlusion_handler.transmit_sky_seg_to_mde_depth_handler()
        _, _, _, _, mde_occl_map, _ = self.occlusion_handler.get_long_distance_occlusions(mde_threshold_mode, use_topography, fastloading)
        vis, fpr, fnr = self.compare_with_groundtruth(self.occlusion_handler.file_handler.get_groundtruth(ImageVariant.CUTOUT), mde_occl_map)
        return vis, fpr, fnr

    def analyze_mde_sky_occlusion(self, scene_number: int, mde_threshold_mode: MDEThresholdMode, use_topography: bool, fastloading=False):
        self.occlusion_handler.load_scene(scene_number)
        self.occlusion_handler.transmit_sky_seg_to_mde_depth_handler()
        _, _, _, combined_occl_map, _, _ = self.occlusion_handler.get_long_distance_occlusions(mde_threshold_mode, use_topography, fastloading)
        vis, fpr, fnr = self.compare_with_groundtruth(self.occlusion_handler.file_handler.get_groundtruth(ImageVariant.CUTOUT), combined_occl_map)
        return vis, fpr, fnr
    
    def analyze_sfm_sky_occlusion(self, scene_number: int, sfm_threshold=10):
        self.occlusion_handler.load_scene(scene_number)
        sfm_occl_map = self.occlusion_handler.get_sfm_occlusion_map(sfm_threshold)
        sky_occl_map = self.occlusion_handler.get_sky_occlusion_map()
        sfm_sky_occl_map = self.occlusion_handler.get_sfm_sky_occlusion_map(sfm_threshold)
        vis1, fpr1, fnr1 = self.compare_with_groundtruth(self.occlusion_handler.file_handler.groundtruth_org, sfm_occl_map)
        vis2, fpr2, fnr2 = self.compare_with_groundtruth(self.occlusion_handler.file_handler.groundtruth_org, sky_occl_map)
        vis3, fpr3, fnr3 = self.compare_with_groundtruth(self.occlusion_handler.file_handler.groundtruth_org, sfm_sky_occl_map)

        return (vis1, fpr1, fnr1), (vis2, fpr2, fnr2), (vis3, fpr3, fnr3)

    def analyze_sfm_mde_sky_occlusion(self, scene_number: int, sfm_threshold=30, mde_threshold_mode=MDEThresholdMode.MIN_UF_MAX, use_topography=True):
        self.occlusion_handler.load_scene(scene_number)
        sfm_sky_mde_occl = self.occlusion_handler.get_combined_occlusion_map(sfm_threshold, mde_threshold_mode, use_topography)

        vis, fpr, fnr = self.compare_with_groundtruth(self.occlusion_handler.file_handler.groundtruth_org, sfm_sky_mde_occl)
        return vis, fpr, fnr
    
    def analyze_sky_cutout_occlusion(self, scene_number: int):
        self.occlusion_handler.load_scene(scene_number)
        sky_occl_map = self.occlusion_handler.get_sky_occlusion_map()

        return self.analyze_cutout_occlusion(sky_occl_map)

    def analyze_sfm_sky_cutout_occlusion(self, scene_number: int, sfm_threshold=30):
        self.occlusion_handler.load_scene(scene_number)
        sfm_sky_occl_map = self.occlusion_handler.get_sfm_sky_occlusion_map(sfm_threshold)

        return self.analyze_cutout_occlusion(sfm_sky_occl_map)

    def analyze_sfm_mde_sky_cutout_occlusion(self, scene_number: int, sfm_threshold=30, mde_threshold_mode=MDEThresholdMode.MIN_UF_MAX, use_topography=True):
        self.occlusion_handler.load_scene(scene_number)
        sfm_sky_mde_occl = self.occlusion_handler.get_combined_occlusion_map(sfm_threshold, mde_threshold_mode, use_topography)

        return self.analyze_cutout_occlusion(sfm_sky_mde_occl)

    def analyze_cutout_occlusion(self, occlusion_map):
        sfm_sky_mde_occl = self.occlusion_handler.file_handler.cut_away(occlusion_map)
        self.occlusion_handler.file_handler.rotation_handler.rotate_and_cut(sfm_sky_mde_occl.astype(np.uint8))
        sfm_sky_mde_cutout_occl = self.occlusion_handler.file_handler.rotation_handler[-1][ImageVariant.CUTOUT]
        groundtruth_cutout = self.occlusion_handler.file_handler.get_groundtruth(ImageVariant.CUTOUT)

        vis, fpr, fnr = self.compare_with_groundtruth(groundtruth_cutout, sfm_sky_mde_cutout_occl)
        return vis, fpr, fnr
    
    """
    Finetune SfM threshold without and in combination with Sky Segmentation.
    """
    def finetune_sfm_threshold(self, scene_number: int):
        """Try out different sfm_thresholds from 0-65 and compare the results with the groundtruth occlusion map. Compute out of this comparison the FPR and
        FNR for every tried threshold."""
        data_holder = self.fine_tune_func(scene_number, occlusion_map_index=0)
        return data_holder

    def finetune_sfm_and_sky_threshold(self, scene_number: int):
        """Try out different sfm_thresholds from 0-65 and compare the results with the groundtruth occlusion map. Compute out of this comparison the FPR and
        FNR for every tried threshold."""
        data_holder = self.fine_tune_func(scene_number, occlusion_map_index=1)

        return data_holder

    def fine_tune_func(self, scene_number, occlusion_map_index):
        self.occlusion_handler.load_scene(scene_number)
        thresholds = [val for val in range(0, 66)]
        data_holder = FineTuningDataHolder(utils.FineTuneMode.SFM)
        last_fpr = 0
        last_fnr = 1
        for sfm_threshold in tqdm(thresholds, total=len(thresholds), desc=f"Trying out different Thresholds for Scene {self.occlusion_handler.scene_number}",
                              leave=True, position=0):
            if last_fpr != 1 or last_fnr != 0:  # fpr starts always at 0 and fnr at 1. If fpr=1 and fnr=0 then the values will stay that way for all
                if occlusion_map_index == 0:
                    occlusion_map = self.occlusion_handler.get_sfm_occlusion_map(sfm_threshold)
                else:
                    occlusion_map = self.occlusion_handler.get_sfm_sky_occlusion_map(sfm_threshold)
                image, fpr, fnr = self.compare_with_groundtruth(self.occlusion_handler.file_handler.groundtruth_org, occlusion_map)
                last_image = image
                last_fpr = fpr
                last_fnr = fnr
                data_holder.add_data_sfm(image, fpr, fnr, sfm_threshold)
            else:
                data_holder.add_data_sfm(last_image, last_fpr, last_fnr, sfm_threshold)

        return data_holder
    
    """
    Compute Statistics and Statistic Visualization
    """
    @staticmethod
    def __get_misclassified_rates(groundtruth, inference):
        """false_positive: too much occluded; false_negative: not enough occluded"""
        if len(groundtruth.shape) == 3:
            groundtruth = groundtruth[:, :, 0]
        if len(inference.shape) == 3:
            inference = inference[:, :, 0]
        positive = np.count_nonzero(inference)
        negative = inference.shape[0] * inference.shape[1] - positive

        true_positive = np.count_nonzero(np.logical_and(groundtruth, inference))
        true_negative = np.count_nonzero(np.logical_and(np.logical_not(groundtruth), np.logical_not(inference)))

        false_positive = positive - true_positive
        false_negative = negative - true_negative

        false_positive_rate = 0 if false_positive == 0 else false_positive / (false_positive + true_negative)
        false_negative_rate = 0 if false_negative == 0 else false_negative / (false_negative + true_positive)

        return false_positive_rate, false_negative_rate

    def compare_with_groundtruth(self, groundtruth, inference):
        """
        Compares two occlusion_masks: the groundtruth with the inferred one. Each pixel is colored in Green, Yellow or Red. Additionally, the False Positive and
        False Negative Rate are drawn on the images.
        If a pixel is green: correct occlusion.      If a pixel is yellow: pixel should be occluded but the inferred occlusion map says different.
        If a pixel is red: pixel should not be occluded but the inferred occlusion map says different.
        """
        if len(groundtruth.shape) == 3:
            groundtruth = groundtruth[:, :, 0]
        if len(inference.shape) == 3:
            inference = inference[:, :, 0]
        result = np.zeros((groundtruth.shape[0], groundtruth.shape[1], 3), dtype=np.uint8)
        result[np.logical_and(groundtruth, inference),:] = (0, 252, 124)  # correct
        result[np.logical_and(np.logical_not(groundtruth), inference),:] = (0, 0, 255)  # wrong occluded
        result[np.logical_and(groundtruth, np.logical_not(inference)),:] = (0, 252, 255)  # wrong not occluded
        fpr, fnr = self.__get_misclassified_rates(groundtruth, inference)
        result = utils.write_text_on_image(result, f"FPR={fpr:.4f}, FNR={fnr:.4f}", text_layer=0)
        return result, fpr, fnr
    
    """
    Visualize Occlusions.
    """
    def visualize_mde_sky_occlusion(self, scene_number: int, mde_threshold_mode: MDEThresholdMode, use_topography: bool, fastloading=False):
        self.occlusion_handler.load_scene(scene_number)
        both_occluded_image, mde_occluded_image, sky_occluded_image, _, _, _ = self.occlusion_handler.get_long_distance_occlusions(mde_threshold_mode,
                                                                                                                                    use_topography, fastloading)
        return both_occluded_image, mde_occluded_image, sky_occluded_image

    def visualize_dataset(self, scene_number: int, fastloading=True):
        """Creates an image sequence for each scene with all needed infromation in it."""
        sfm_threshold = 30
        mde_threshold_mode = MDEThresholdMode.MIN_UF_MAX
        use_topography = True
        self.occlusion_handler.load_scene(scene_number)
        fh = self.occlusion_handler.file_handler
        screen = fh.get_screen(ImageVariant.FULL)
        back = fh.background_org
        ground = fh.groundtruth_org
        
        sfm_15_occl = self.occlusion_handler.get_sfm_occlusion_map(sfm_threshold=15)
        sky_occl_map = self.occlusion_handler.get_sky_occlusion_map()
        sfm_15_comp, _, _ = self.compare_with_groundtruth(self.occlusion_handler.file_handler.groundtruth_org, sfm_15_occl)
        sfm = utils.rescale_depth_images(fh.sfm_depth)
        sky_seg = np.logical_not(sky_occl_map)
        sky_comp, _, _ = self.compare_with_groundtruth(ground, sky_occl_map)
        sky_occl_map = utils.rescale_depth_images(sky_occl_map)

        sfm_30_occl = self.occlusion_handler.get_sfm_occlusion_map(sfm_threshold=30)
        sfm_sky_occl = self.occlusion_handler.get_sky_occlusion_map()

        sfm_sky_comp, _, _ = self.compare_with_groundtruth(ground, sfm_sky_occl)

        mde_depth = utils.rescale_depth_images(self.occlusion_handler.mde_handler.get_mde_depth(fastloading))

        _, _, _, _, mde_occl_map, _ = self.occlusion_handler.get_long_distance_occlusions(mde_threshold_mode, use_topography, fastloading)
        mde_comp, _, _ = self.compare_with_groundtruth(self.occlusion_handler.file_handler.get_groundtruth(ImageVariant.CUTOUT), mde_occl_map)

        _, _, _, mde_sky_occl_map, _, _ = self.occlusion_handler.get_long_distance_occlusions(mde_threshold_mode, use_topography, fastloading)
        mde_sky_comp, _, _ = self.compare_with_groundtruth(self.occlusion_handler.file_handler.get_groundtruth(ImageVariant.CUTOUT), mde_sky_occl_map)

        sfm_sky_occl_cutout = fh.cut_away(sfm_sky_occl)
        fh.rotation_handler.rotate_and_cut(sfm_sky_occl_cutout.astype(np.uint8))
        sfm_sky_occl_cutout = fh.rotation_handler[-1][ImageVariant.CUTOUT]

        small_occluded_image = self.occlusion_handler.visualize_cutout_occlusion(np.logical_or(mde_sky_occl_map, sfm_sky_occl_cutout))

        sfm_mde_sky_occl = self.occlusion_handler.get_combined_occlusion_map(sfm_threshold, mde_threshold_mode, use_topography)

        sfm_sky_occluded_image = self.occlusion_handler.visualize_original_size_occlusion(sfm_sky_occl)

        big_occluded_image = self.occlusion_handler.visualize_original_size_occlusion(sfm_mde_sky_occl)
        big_comp, _, _ = self.compare_with_groundtruth(ground, sfm_mde_sky_occl)

        small_concat = [mde_depth, mde_comp, mde_sky_comp, small_occluded_image]

        topo_depth = utils.rescale_depth_images(-1*(fh.get_topo(ImageVariant.FULL)-1))
        topo_depth = utils.replace_color(topo_depth, utils.white, utils.cyan)

        concat = [screen, back, ground, topo_depth, sfm, sky_occl_map, sfm_15_comp, sky_comp, sfm_sky_comp, sfm_sky_occluded_image,
                  utils.concat_images_col(small_concat, n_cols=2), big_occluded_image, big_comp]
        result = utils.concat_images(concat, mode=ConcatMode.HORIZONTAL)

        return result
