import cv2
import numpy as np

# TODO: topo_depth_no_buildings
# TODO: SLAM
# TODO: mono
# TODO: FPR, FNR weight


class SfMHandler:
    """
    Handles everything related to the Structure from Motion(SfM) approach.
    OcclusionHandler uses the SfMHandler to request the SfM occlusion map.
    """

    def __init__(self, sfm_depth, background_image, windturbine_distance):
        self.sfm_depth = sfm_depth
        self.background_image = background_image
        self.windturbine_distance = windturbine_distance

    def get_sfm_occlusion_map(self, sfm_threshold=10):
        """
        Computes the sfm occlusion map by using the sfm_threshold and the given sfm depth data. Used by OcclusionHandler.
        :param sfm_threshold: threshold after which the SfM depth values are classified as background (are ignored).
        :return: SfM Occlusion Map being a boolean array with np.array(newH,newW,3; dtype=np.uint8).
                1=Occlude wind turbine, 0=Don't occlude wind turbine
        """
        sfm_occl = self.get_sfm_occlusion(sfm_threshold)
        newH, newW = self.background_image.shape[:2]
        sfm_occlusion_map = cv2.resize(np.stack([sfm_occl, sfm_occl, sfm_occl], axis=2).astype(np.uint8), (newW, newH))
        return sfm_occlusion_map

    def get_sfm_occlusion(self, sfm_threshold=10):
        """
        Helper function to compute the SfM occlusion.
        :param sfm_threshold: threshold after which the SfM depth values are classified as background (are ignored).
        :return: sfm occlusion boolean array np.array(h,w).
        """
        sfm_occlusion = self.sfm_depth[:, :, 0] < self.windturbine_distance  # sfm_depth saved in meters - True if pixel is in front of Scene-Object
        sfm_occlusion[self.sfm_depth[:, :, 0] >= sfm_threshold] = False  # the higher the sfm_threshold the lower the confidence
        return sfm_occlusion

