import numpy as np

from Handler.FileHandler import FileHandler
from Handler.MDEHandler import MDEHandler
from Handler.SfMHandler import SfMHandler
from Handler.SkyHandler import SkyHandler
from Handler.utils import MDEThresholdMode, ImageVariant


class OcclusionHandler:
    """
    Is the core which combines the File, SfM, Sky, and MDE handler.
    Gives access to all needed occlusion maps.
    """

    def __init__(self, path_prefix, image_format=".png", model="./DenseDepth/nyu.h5"):
        """
        Initializes the OcclusionHandler for a specified Sequence, by preparing the FileHandler.
        :param path_prefix: path to the images of the Sequence.
        :param image_format:
        :param model:
        """
        self.file_handler = FileHandler(path_prefix, image_format, model)

        self.scene_number = None
        self.mde_handler: MDEHandler = None
        self.sfm_handler: SfMHandler = None
        self.sky_handler: SkyHandler = None

    def load_scene(self, image_number: int):
        """
        Loads a specified Scene from the Sequence.
        :param image_number: integer of the Scene to load.
        """
        self.scene_number = image_number
        fh = self.file_handler
        fh.load_scene(image_number)
        self.sfm_handler = SfMHandler(fh.sfm_depth, fh.background_org, fh.windturbine_distance)
        self.sky_handler = SkyHandler(fh.background_org, fh.points, fh.get_topo(ImageVariant.FULL), fh.windturbine_distance)
        self.mde_handler = MDEHandler(fh.path_prefix, image_number, fh.model, fh.get_background(ImageVariant.CUTOUT), fh.get_groundtruth(ImageVariant.CUTOUT), 
                                      fh.get_topo(ImageVariant.CUTOUT), windturbine_distance=fh.windturbine_distance, 
                                      topo_depth_fix=fh.rotation_handler[-1][ImageVariant.CUTOUT])

    def get_combined_occlusion_map(self, sfm_threshold, mde_threshold_mode, use_topography, fastloading=True):
        """
        Acquire the combined occlusion map.
        :param sfm_threshold: threshold after which the SfM depth values are discarded.
        :param mde_threshold_mode: Used to specify which Thresholding Methods the MDEHandler shoudld use.
        :param use_topography: if true then the MDEHandler uses topographic Region Selection.
        :param fastloading: if true then the MDEHandler will look if the MDE depth was computed before and uses it instead of using the neural network.
        :return: combination of sfm, mde and sky occlusion map np.array(h,w,3) only first channel is needed.
        """
        sfm_sky_occl_map = self.get_sfm_sky_occlusion_map(sfm_threshold)

        self.transmit_sky_seg_to_mde_depth_handler()
        _, _, _, mde_sky_cutout_occl, _, _ = self.get_long_distance_occlusions(mde_threshold_mode, use_topography, fastloading)
        mde_sky_occl = self.__adjust_cutout_occlusion_map_to_camera_size(mde_sky_cutout_occl, sfm_sky_occl_map)
        sfm_mde_sky_occl = np.logical_or(sfm_sky_occl_map, mde_sky_occl)

        return sfm_mde_sky_occl
        
    def get_sfm_occlusion_map(self, sfm_threshold=10):
        """
        Acquire the SfM occlusion map.
        :param sfm_threshold: threshold after which the SfM depth values are discarded.
        :return: SfM occlusion map np.array(h,w,3) only first channel is needed.
        """
        return self.sfm_handler.get_sfm_occlusion_map(sfm_threshold)
    
    def get_sky_occlusion_map(self):
        """
        Acquire the Sky occlusion map.
        :return: Sky occlusion map np.array(h,w,3) only first channel is needed.
        """
        return self.sky_handler.get_sky_occlusion_map()
    
    def get_sfm_sky_occlusion_map(self, sfm_threshold=10):
        """
        Correct SfM occlusion map with sky segmentation.
        :param sfm_threshold: threshold after which the SfM depth values are discarded.
        :return: SfM Sky occlusion map np.array(h,w,3) only first channel is needed.
        """
        sfm_occl_map = self.get_sfm_occlusion_map(sfm_threshold)
        sky_occl_map = self.get_sky_occlusion_map()
        sfm_sky_occl_map = np.logical_and(sfm_occl_map, sky_occl_map)
        return sfm_sky_occl_map

    def get_mde_occlusion_map(self, mde_threshold_mode: MDEThresholdMode, use_topography: bool, fastloading=False):
        """
        Acquire the MDE occlusion map.
        :param mde_threshold_mode: Used to specify which Thresholding Methods the MDEHandler shoudld use.
        :param use_topography: if true then the MDEHandler uses topographic Region Selection.
        :param fastloading: if true then the MDEHandler will look if the MDE depth was computed before and uses it instead of using the neural network.
        :return: MDE occlusion map np.array(cutoutH,cutoutW,3) only first channel is needed.
        """
        mde_occlusion_map = self.mde_handler.get_mde_occlusion_map(mde_threshold_mode, use_topography, fastloading)
        return mde_occlusion_map

    def get_long_distance_occlusions(self, mde_threshold_mode: MDEThresholdMode, use_topography: bool, fastloading=False):
        """
        Compute everything related to the long distance occlusion.
        :param mde_threshold_mode: Used to specify which Thresholding Methods the MDEHandler shoudld use.
        :param use_topography: if true then the MDEHandler uses topographic Region Selection.
        :param fastloading: if true then the MDEHandler will look if the MDE depth was computed before and uses it instead of using the neural network.
        :return:
        """
        mde_occl_map = self.get_mde_occlusion_map(mde_threshold_mode, use_topography, fastloading)
        sky_occl_map = self.get_sky_occlusion_map()
        sky_occl_map_adjusted = self.file_handler.cut_away(sky_occl_map.astype(np.uint8))
        self.file_handler.rotation_handler.rotate_and_cut(sky_occl_map_adjusted)
        sky_occl_map_cutout = self.file_handler.rotation_handler[-1][ImageVariant.CUTOUT].astype(bool)
        sky_segmentation_cutout = np.logical_not(sky_occl_map_cutout)
        mde_sky_occl_map = mde_occl_map.copy()
        mde_sky_occl_map[sky_segmentation_cutout] = False

        sky_occluded_image = self.visualize_cutout_occlusion(sky_occl_map_cutout)
        mde_occluded_image = self.visualize_cutout_occlusion(mde_occl_map)
        mde_sky_occluded_image = self.visualize_cutout_occlusion(mde_sky_occl_map)
        return mde_sky_occluded_image, mde_occluded_image, sky_occluded_image, mde_sky_occl_map, mde_occl_map, sky_occl_map_cutout

    # def get_topo_and_occlusion_map(self, mde_threshold_mode: MDEThresholdMode, use_topography: bool, fastloading=False):
    #     return self.mde_handler.get_topo_and_mde_occlusion_map(mde_threshold_mode, use_topography, fastloading)

    """
    Visualize the wind turbine occlusion with different occlusion maps on the original camera image size.
    """
    def visualize_sfm_sky_occlusion(self, sfm_threshold=10):
        sfm_sky_occlusion_map = self.get_sfm_sky_occlusion_map(sfm_threshold)
        occluded_image = self.visualize_original_size_occlusion(sfm_sky_occlusion_map)
        return occluded_image

    def visualize_sfm_occlusion(self, sfm_threshold=10):
        sfm_occl_map = self.get_sfm_occlusion_map(sfm_threshold)
        occluded_image = self.visualize_original_size_occlusion(sfm_occl_map)
        return occluded_image

    def visualize_sky_occlusion(self):
        sky_occl_map = self.get_sky_occlusion_map()
        occluded_image = self.visualize_original_size_occlusion(sky_occl_map)
        return occluded_image

    def visualize_original_size_occlusion(self, occlusion_map):
        occlusion_map = occlusion_map[:, :, 0].astype(bool)
        back_org = self.file_handler.background_org
        screen = self.file_handler.get_screen(ImageVariant.FULL)
        width_diff = back_org.shape[1] - screen.shape[1]

        occluded_image = back_org.copy()
        occluded_image[:, width_diff // 2:back_org.shape[1] - width_diff // 2] = screen
        occluded_image[occlusion_map] = back_org[occlusion_map]
        return occluded_image

    def visualize_combined_occlusion_full(self, sfm_threshold=10, mde_threshold_mode=MDEThresholdMode.UF, use_topography=False, fastloading=False):
        sfm_sky_occlusion_map = self.get_sfm_sky_occlusion_map(sfm_threshold)
        mde_occlusion_map = self.get_mde_occlusion_map(mde_threshold_mode, use_topography, fastloading)
        sky_occlusion_map = self.get_sky_occlusion_map()

        camera_size_mde_occlusion_map = self.__adjust_cutout_occlusion_map_to_camera_size((mde_occlusion_map, sfm_sky_occlusion_map))

        mde_sky_occlusion_map = camera_size_mde_occlusion_map.copy()
        mde_sky_occlusion_map[sky_occlusion_map] = False

        global_occlusion_map = np.logical_or(sfm_sky_occlusion_map.astype(bool), mde_sky_occlusion_map.astype(bool))
        occluded_image = self.visualize_original_size_occlusion(global_occlusion_map)
        return occluded_image

    def __adjust_cutout_occlusion_map_to_camera_size(self, mde_sky_cutout_occl, sfm_sky_occl):
        screen_size_mde_occlusion_map = self.file_handler.rotation_handler.reverse_rotate_and_cut(mde_sky_cutout_occl)
        camera_size_mde_occlusion_map = np.zeros_like(sfm_sky_occl, dtype=bool)
        screen_width = screen_size_mde_occlusion_map.shape[1]
        camera_width = sfm_sky_occl.shape[1]
        diff_width = camera_width - screen_width
        camera_size_mde_occlusion_map[:, diff_width // 2:camera_width - diff_width // 2] = screen_size_mde_occlusion_map.astype(bool)
        return camera_size_mde_occlusion_map

    """
    Visualize the wind turbine occlusion with different occlusion maps on the cutout of the camera image.
    """
    def visualize_cutout_occlusion(self, occlusion_map):
        back = self.file_handler.get_background(ImageVariant.CUTOUT)
        screen = self.file_handler.get_screen(ImageVariant.CUTOUT)
        occluded_image = screen.copy()

        occluded_image[occlusion_map] = back[occlusion_map]
        return occluded_image

    def visualize_occlusion_long_distance(self, mde_threshold_mode=MDEThresholdMode.UF, use_topography=False, fastloading=False):
        mde_occlusion_map = self.mde_handler.get_mde_occlusion_map(mde_threshold_mode, use_topography, fastloading)
        sky_segmentation = np.logical_not(self.get_sky_occlusion_map())

        screen_width = self.file_handler.get_screen(ImageVariant.FULL).shape[1]
        camera_width = sky_segmentation.shape[1]
        diff_width = camera_width - screen_width
        screen_size_sky_occlusion_map = sky_segmentation[:, diff_width // 2:camera_width - diff_width // 2].astype(np.uint8)
        index = self.file_handler.rotation_handler.rotate_and_cut(screen_size_sky_occlusion_map)
        cutout_sky_segmentation = self.file_handler.rotation_handler[index][ImageVariant.CUTOUT]

        mde_sky_occlusion_map = mde_occlusion_map.copy()
        mde_sky_occlusion_map[cutout_sky_segmentation] = False

        occluded_image = self.visualize_cutout_occlusion(mde_sky_occlusion_map)
        return occluded_image

    def visualize_global_occlusion_cutout(self, sfm_threshold=10, mde_threshold_mode=MDEThresholdMode.UF, use_topography=False, fastloading=False):
        sfm_sky_occlusion_map = self.get_sfm_sky_occlusion_map(sfm_threshold)
        mde_occlusion_map = self.mde_handler.get_mde_occlusion_map(mde_threshold_mode, use_topography, fastloading)

        screen_width = self.file_handler.get_screen(ImageVariant.FULL).shape[1]
        camera_width = sfm_sky_occlusion_map.shape[1]
        diff_width = camera_width - screen_width

        screen_size_sfm_sky_occlusion_map = sfm_sky_occlusion_map[:, diff_width // 2:camera_width - diff_width // 2].astype(bool)
        index = self.file_handler.rotation_handler.rotate_and_cut(screen_size_sfm_sky_occlusion_map)
        cutout_sfm_sky_occlusion_map = self.file_handler.rotation_handler[index]

        global_occlusion_map = np.logical_or(cutout_sfm_sky_occlusion_map.astype(bool), mde_occlusion_map.astype(bool))

        occluded_image = self.visualize_cutout_occlusion(global_occlusion_map)
        return occluded_image

    def transmit_sky_seg_to_mde_depth_handler(self):
        """
        Helper function during analysis.
        :return:
        """
        sky_segmentation = np.logical_not(self.get_sfm_sky_occlusion_map())
        sky_segmentation = self.file_handler.cut_away(sky_segmentation.astype(np.uint8))
        self.file_handler.rotation_handler.rotate_and_cut(sky_segmentation)
        sky_segmentation_cutout = self.file_handler.rotation_handler[-1][ImageVariant.CUTOUT].astype(bool)
        self.mde_handler.sky_seg = sky_segmentation_cutout
        self.mde_handler.sky_seg_set = True