import os.path

from DenseDepth.utils import predict

import cv2
import numpy as np
from Handler import utils
from Handler.utils import Vec2DInt, MDEThresholdMode


class MDEHandler:
    """
    Handles everything related to the Monocular Depth Estimation approach and the Region Selection and Thresholding Methods..
    OcclusionHandler uses the MDEHandler to request the MDE occlusion map.
    """

    def __init__(self, path_prefix, scene_number, model, background_cutout, groundtruth_cutout, topo_depth, windturbine_distance, topo_depth_fix):
        self.path_prefix = path_prefix
        self.scene_number = scene_number
        self.model = model
        self.background_cutout = background_cutout
        self.groundtruth_cutout = groundtruth_cutout
        self.topo_depth = topo_depth
        self.topo_depth_fix = topo_depth_fix
        self.windturbine_distance = windturbine_distance

        self.sky_seg = None
        self.sky_seg_set = False

    def get_mde_occlusion_map(self, mde_threshold_mode: MDEThresholdMode, use_topography: bool, fastloading=False):
        """
        Compute the MDE occlusion map.
        :param mde_threshold_mode: Used to specify which Thresholding Methods the MDEHandler shoudld use.
        :param use_topography: if true then the MDEHandler uses topographic Region Selection.
        :param fastloading: if true then the MDEHandler will look if the MDE depth was computed before and uses it instead of using the neural network.
        :return: MDE occlusion map np.array(cutoutH,cutoutW,3) only first channel is needed.
        """
        topo_occlusion_map = self.__get_topo_occlusion_map(self.topo_depth, self.windturbine_distance)
        if topo_occlusion_map.sum() == 0:   # no topo depth data available because of low precision
            topo_occlusion_map = self.topo_depth_fix > 0
        mde_occlusion_map = self.__get_mde_occlusion_map(mde_threshold_mode, use_topography, topo_occlusion_map, fastloading)
        return mde_occlusion_map

    def get_mde_depth(self, fastloading=False):
        """
        Use neural network to compute the mde depth
        :param fastloading: if true then the MDEHandler will look if the MDE depth was computed before and uses it instead of using the neural network.
        :return: mde depth map np.array(cutoutH,cutoutW,3) only first channel is needed.
        """
        return self.__compute_mde_depth_map(self.background_cutout, self.path_prefix, self.scene_number, fastloading=fastloading)

    def __get_mde_occlusion_map(self, mde_threshold_mode: MDEThresholdMode, use_topography: bool, topo_occlusion_map=None, fastloading=False):
        """
        Use neural network to compute the mde depth
        :param mde_threshold_mode: Used to specify which Thresholding Methods the MDEHandler shoudld use.
        :param use_topography: if true then the MDEHandler uses topographic Region Selection.
        :param topo_occlusion_map: occlusion map used for topographic Region Selection.
        :param fastloading: if true then the MDEHandler will look if the MDE depth was computed before and uses it instead of using the neural network.
        :return: mde depth map np.array(cutoutH,cutoutW,3) only first channel is needed.
        """
        mde_depth = self.__compute_mde_depth_map(self.background_cutout, self.path_prefix, self.scene_number, fastloading=fastloading)

        # Select region of values which are probably foreground.
        if use_topography:
            values = mde_depth[topo_occlusion_map[:,:,0], 0]
        else:
            point = Vec2DInt(y=(0.8 * mde_depth.shape[0]), x=(0.5 * mde_depth.shape[1]))  # TODO use given points
            values = mde_depth[point.y:, point.x - 1:point.x + 2, 0]

        # Select a threshold out of the selected region according to selected thresholding selection method.
        if mde_threshold_mode == MDEThresholdMode.UF:
            mde_threshold = self.uf_get_mde_threshold(values)
        elif mde_threshold_mode == MDEThresholdMode.QUARTILE3:
            mde_threshold = self.quartil3_get_mde_threshold(values)
        elif mde_threshold_mode == MDEThresholdMode.MIN_UF_MAX:
            mde_threshold = self.min_uf_max_get_mde_threshold(values)
        elif mde_threshold_mode == MDEThresholdMode.HAND:  # Use User to define Threshold or use old threshold which was defined by a user in the past
            hand_threshold_path = f"{self.path_prefix}hand_threshold_{self.scene_number}.txt"
            if os.path.exists(hand_threshold_path):
                with open(hand_threshold_path, "r") as file:
                    mde_threshold = float(file.readline())
                    # with open("./HAND.txt", "a") as file:
                    #     file.write(f"{mde_threshold}, ")
            else:
                mde_threshold = self.hand_get_mde_threshold(mde_depth, utils.draw_mask_on_image(self.background_cutout, self.groundtruth_cutout[:, :, 0],
                                                                                                  color=(0, 0, 255)), self.sky_seg, self.sky_seg_set)
                with open(hand_threshold_path, "w") as file:
                    file.write(f"{mde_threshold}")
        else:  # MDEThresholdMode.MAX:
            mde_threshold = self.default_get_mde_threshold(values)

        # create occlusion map according to selected threshold
        mde_occlusion_map = mde_depth[:, :, 0] < mde_threshold
        return np.stack([mde_occlusion_map, mde_occlusion_map, mde_occlusion_map], axis=2)

    @staticmethod
    def __get_topo_occlusion_map(topo_depth, windturbine_distance):
        """
        Compute the topo/DEM occlusion map.
        :param topo_depth: topo/DEM depth data.
        :param windturbine_distance: integer, distance of the wind turbine towards the user.
        :return: topo/DEM occlusion map np.array(h,w,3) only first channel is needed.
        """
        topo_occlusion_map = topo_depth[:, :, 0] > 1 / windturbine_distance  # topodepth saved as 1/distance
        return np.stack([topo_occlusion_map, topo_occlusion_map, topo_occlusion_map], axis=2)

    def __compute_mde_depth_map(self, image, path_prefix, scene_number, fastloading=False):
        """
        Computes the mde depth or loads it from an existing file. After computation, it is stored in a file, so instead of recomputing it next time it can be
        loaded faster from that file.
        :param image: image which is fed into the neural network np.array(h,w,3).
        :param path_prefix: path of the Sequence such that the mde depth can be loaded or stored in the correct directory.
        :param scene_number: integer, number of the scene so that the stored file can be associated with the correct Scene.
        :param fastloading: if true then the MDEHandler will look if the MDE depth was computed before and uses it instead of using the neural network.
        :return: mde depth map np.array(h,w,3) only first channel is needed.
        """
        if fastloading:
            try:
                return np.load(f"{path_prefix}mde_{scene_number}.npy")
            except FileNotFoundError:
                pass
        inputs = np.array([cv2.resize(image, (640, 480))]) / 255  # Adjust input image to fit model
        output = predict(self.model, inputs)[0]
        mde_depth_map = cv2.resize(output.astype(np.float32), (image.shape[1], image.shape[0]))  # Adjust output to fit background_cutout
        mde_depth_map = np.stack([mde_depth_map, mde_depth_map, mde_depth_map], axis=2)  # Adjust output to fit cv2
        np.save(f"{path_prefix}mde_{scene_number}.npy", mde_depth_map)
        return mde_depth_map

    """
    Thresholding Methods.
    """
    @staticmethod
    def default_get_mde_threshold(values):
        """The easiest way to get the threshold."""
        try:
            max = values.max()
        except:
            max = 0
        # with open("./MAX.txt", "a") as file:
        #     file.write(f"{max}, ")
        return max

    @staticmethod
    def uf_get_mde_threshold(values: np.array):
        """Calculates the interquartile range and uses the upper fence to discard outliers"""
        vals = values.copy()
        vals.sort()
        q3 = np.quantile(vals, 0.75)
        q1 = np.quantile(vals, 0.25)
        iqr = q3 - q1
        upper_fence = q3 + (1.5 * iqr)
        # with open("./UF.txt", "a") as file:
        #     file.write(f"{upper_fence}, ")
        return upper_fence

    @staticmethod
    def min_uf_max_get_mde_threshold(values: np.array):
        """Calculates the interquartile range and uses the upper fence to discard outliers"""
        vals = values.copy()
        vals.sort()
        q3 = np.quantile(vals, 0.75)
        q1 = np.quantile(vals, 0.25)
        iqr = q3 - q1
        upper_fence = q3 + (1.5 * iqr)
        max_val = vals.max()
        # with open("./MIN.txt", "a") as file:
        #     file.write(f"{min(max_val, upper_fence)}, ")
        return min(max_val, upper_fence)

    @staticmethod
    def quartil3_get_mde_threshold(values):
        """Calculates the third(0.75) quartil and uses it to discard more outliers than the upper fence."""
        vals = values.copy()
        vals.sort()
        q3 = np.quantile(vals, 0.75)
        # with open("./Q3.txt", "a") as file:
        #     file.write(f"{q3}, ")
        return q3

    @staticmethod
    def hand_get_mde_threshold(mde_depth_map, screen_image, sky_seg, sky_seg_set):
        """Use a User to set the threshold value by hand to visualize the maximum possible output result."""
        mde_depth = mde_depth_map[:, :, 0]
        window_name = "MDE Occlusion"
        cv2.namedWindow(window_name)
        mde_max = mde_depth.max()

        def on_trackbar(val):
            threshold = val / 100 * mde_max
            mask = mde_depth < threshold
            if sky_seg_set:
                mask[sky_seg[:,:,0]] = False
            result = utils.draw_mask_on_image(screen_image, mask)
            cv2.imshow(window_name, result)

        cv2.createTrackbar('Threshold val/100*mde_max', window_name, 0, 100, on_trackbar)
        on_trackbar(0)
        cv2.waitKey()
        threshold = cv2.getTrackbarPos('Threshold val/100*mde_max', window_name) / 100 * mde_max
        cv2.destroyAllWindows()
        return threshold