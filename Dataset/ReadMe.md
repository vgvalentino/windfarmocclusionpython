The Folder Dataset+Occlusion contains the Scenes and the occlusion result using SfM+MDE+Sky with 30m SfM Threshold and topographic Region Selection and MIN(UF,MAX) Thresholding Method.

The Folder Dataset+Occlusion+Intermediate_Steps contains the Scenes and occlusion results as well as the comparison maps of different approaches. Can be used for deeper analysis.


The images have four different prefixes: 
paul - representing the Paulusstraße Sequence
inf - representing the Informatikzentrum Sequence
koen - representing the Königshügel Sequence
lous - representing the Lousberg Sequence


Note: Some Scenes from the Informatikzentrum Sequence contain erroneous Screen Images (green, bluish images). However, this is only problematic for the visualization of the occlusion and did not influence the evaluation.