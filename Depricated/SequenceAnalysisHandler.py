# import glob
# import math
#
# import cv2
# import numpy as np
# from tqdm import tqdm
#
# from Depricated import utils
# from Depricated.AnalysisHandler import AnalysisHandler
# from Depricated.SceneHandler import SceneHandler
# from Depricated.utils import ImageVariant, ConcatMode
#
#
# class SequenceAnalysisHandler:
#
#     def __init__(self, scene_handler: SceneHandler, start_number=None, end_number=None):
#         self.sceneHandler = scene_handler
#         if start_number is not None and end_number is not None:
#             self.start_number = start_number
#             self.end_number = end_number
#         else:
#             files = glob.glob(f"{scene_handler.path_prefix}background_*")
#             numbers = [int(file.split("_")[-1].split(scene_handler.image_format)[0]) for file in files]
#             self.start_number = min(numbers)
#             self.end_number = max(numbers)
#
#     def visualize_occlusion_masks_sequence(self, use_topography, mono_threshold_mode, slam_treshold_distance):
#         concat = []
#         for i in range(self.start_number, self.end_number + 1):
#             self.sceneHandler.load_scene(i, fastloading=True)
#             self.sceneHandler.set_occlusion_maps(use_topography, mono_threshold_mode, slam_treshold_distance)
#             analysis = AnalysisHandler(self.sceneHandler, i)
#             concat.append(analysis.visualize_occlusion_masks().astype(np.uint8))
#         return utils.concat_images(concat, mode=ConcatMode.VERT)
#
#     def visualize_slam_depth_image_sequence(self, n_cols=4):
#         concat = []
#         for i in tqdm(range(self.start_number, self.end_number + 1), total=self.end_number + 1 - self.start_number,
#                       desc="SequenceAnalysisHandler: Visualizing SLAM Depth Image Sequence"):
#             self.sceneHandler.load_scene(i, fastloading=True)
#             back = self.sceneHandler.getBackground()[ImageVariant.FULL]
#             slam = self.sceneHandler.getSLAM()[ImageVariant.FULL]
#             print(np.min(slam[:, :, 0]), np.max(slam[:, :, 0]))
#             slam_depth = utils.rescale_depth_images(np.stack([slam[:, :, 0], slam[:, :, 0], slam[:, :, 0]], axis=2))
#             concat.append(utils.concat_images([back, slam_depth], mode=ConcatMode.HORIZONTAL))
#         concat_vert = []
#         for i in range(math.ceil(len(concat) / n_cols)):
#             row = concat[i * n_cols:(i + 1) * n_cols]
#             if len(row) > 0:
#                 for i in range(n_cols - len(row)):
#                     row.append(np.full_like(row[0], fill_value=255))
#                 concat_vert.append(utils.concat_images(row, mode=ConcatMode.HORIZONTAL))
#         return utils.concat_images(concat_vert, mode=ConcatMode.VERT)
#
#     def visualize_mono_depth_image_sequence(self, n_cols=4):
#         concat = []
#         for i in tqdm(range(self.start_number, self.end_number + 1), total=self.end_number + 1 - self.start_number,
#                       desc="SequenceAnalysisHandler: Visualizing Mono Depth Image Sequence"):
#             self.sceneHandler.load_scene(i, fastloading=True)
#             back = self.sceneHandler.getBackground()[ImageVariant.CUTOUT]
#             mono_depth = utils.rescale_depth_images(np.stack([self.sceneHandler.monocular_depth_map[:, :, 0], self.sceneHandler.monocular_depth_map[:, :, 0],
#                                                               self.sceneHandler.monocular_depth_map[:, :, 0]], axis=2))
#             concat.append(utils.concat_images([back, mono_depth], mode=ConcatMode.HORIZONTAL))
#         concat_vert = []
#         for i in range(math.ceil(len(concat) / n_cols)):
#             row = concat[i * n_cols:(i + 1) * n_cols]
#             if len(row) > 0:
#                 for i in range(n_cols - len(row)):
#                     row.append(np.full_like(row[0], fill_value=255))
#                 concat_vert.append(utils.concat_images(row, mode=ConcatMode.HORIZONTAL))
#         return utils.concat_images(concat_vert, mode=ConcatMode.VERT)
#
#     def visualize_missclassification_sequence(self, use_topography, mono_threshold_mode, slam_treshold_distance):
#         concat = []
#         for i in range(self.start_number, self.end_number + 1):
#             self.sceneHandler.load_scene(i, fastloading=True)
#             self.sceneHandler.set_occlusion_maps(use_topography, mono_threshold_mode, slam_treshold_distance)
#             analysis = AnalysisHandler(self.sceneHandler, i)
#             scene_images, fprs, fnrs = analysis.visualize_missclassification()
#             concat.append(scene_images.astype(np.uint8))
#         return utils.concat_images(concat, mode=ConcatMode.HORIZONTAL)
#
#     def visualize_slam_missclassification_sequence(self, slam_treshold_distance):
#         concat = []
#         for i in range(self.start_number, self.end_number + 1):
#             self.sceneHandler.load_scene(i, slam_treshold_distance=slam_treshold_distance, fastloading=True)
#             analysis = AnalysisHandler(self.sceneHandler, i)
#             slam_depth = analysis.sceneHandler.getSLAM()[ImageVariant.FULL][:, :, 0]
#             slam_occlusion_map = np.array(
#                 slam_depth < self.sceneHandler.windturbine_distance)  # slamdepth saved in meters - True if pixel is in front of Scene-Object
#             slam_occlusion_map[slam_depth >= slam_treshold_distance] = False  # the higher the slam_distance the lower the confidence
#             slam_fpr_fnr, fpr, fnr = analysis.visualize_comparison_with_groundtruth(analysis.groundtruth_variants[ImageVariant.FULL][:, :, 0] / 255,
#                                                                                     slam_occlusion_map[:, :, 0])
#             concat.append(slam_fpr_fnr.astype(np.uint8))
#         return utils.concat_images(concat, mode=ConcatMode.HORIZONTAL)
#
#     def fine_tune_sfm_threshold_sequence(self):
#         finetuning_dataholders = []
#         for i in tqdm(range(self.start_number, self.end_number + 1), desc="SeqenceAnalysisHandler: Finetuning SLAM Sequence:", position=1):
#             self.sceneHandler.load_scene(i, fastloading=True)
#             analysis = AnalysisHandler(self.sceneHandler, i)
#             dataholder = analysis.finetune_slam_threshold()
#             dataholder.images = None  # Otherwise we get a Out of memory exception for a big sequence
#             finetuning_dataholders.append(dataholder)
#         return finetuning_dataholders
#
#     def finetune_mono_threshold_mode_sequence(self):
#         """Try out different mono_threhold modes. In the first for loop the topography is not used and all modes are tried. In the second for loop the modes use
#         the topography as region selection to find the threshold."""
#         finetuning_dataholders = []
#         for i in tqdm(range(self.start_number, self.end_number + 1), desc="SeqenceAnalysisHandler: Finetuning SLAM Sequence:", leave=True, position=0):
#             self.sceneHandler.load_scene(i, fastloading=True)
#             analysis = AnalysisHandler(self.sceneHandler, i)
#             dataholder = analysis.finetune_mono_threshold_mode()
#             # dataholder.images = None # Otherwise we get a Out of memory exception for a big sequence
#             finetuning_dataholders.append(dataholder)
#         return finetuning_dataholders
#
#     def fine_tune_slam_threshold_with_sky_seg_sequence(self):
#         finetuning_dataholders = []
#         for i in tqdm(range(self.start_number, self.end_number + 1), desc="SeqenceAnalysisHandler: Finetuning SLAM Sequence:"):
#             self.sceneHandler.load_scene(i, fastloading=True)
#             analysis = AnalysisHandler(self.sceneHandler, i)
#             dataholder = analysis.finetune_slam_threshold_with_sky_segmentation()
#             # dataholder.images = None  # Otherwise we get a Out of memory exception for a big sequence
#             finetuning_dataholders.append(dataholder)
#         return finetuning_dataholders
#
#     # def try_sky_detection(self, n_cols=4):
#     #     concat = []
#     #     for i in tqdm(range(self.start_number, self.end_number + 1), total=self.end_number + 1 - self.start_number,
#     #                   desc="SequenceAnalysisHandler: Visualizing Mono Depth Image Sequence"):
#     #         self.sceneHandler.load_scene(i, fastloading=True)
#     #         segmentation = self.sceneHandler.get_sky_segmentation()
#     #
#     #         segmentation, sky_mask, ground_mask = np.stack([segmentation, segmentation, segmentation], axis=2)
#     #         # back = save
#     #         concat.append(utils.concat_images([utils.draw_mask_on_image(utils.draw_mask_on_image(back, sky_mask), ground_mask),
#     #                                            cv2.resize(segmentation * 255, (back.shape[1], back.shape[0]))], mode=ConcatMode.HORIZONTAL))
#     #     concat_vert = []
#     #     for i in range(math.ceil(len(concat) / n_cols)):
#     #         row = concat[i * n_cols:(i + 1) * n_cols]
#     #         if len(row) > 0:
#     #             for i in range(n_cols - len(row)):
#     #                 row.append(np.full_like(row[0], fill_value=255))
#     #             concat_vert.append(utils.concat_images(row, mode=ConcatMode.HORIZONTAL))
#     #     return utils.concat_images(concat_vert, mode=ConcatMode.VERT)
