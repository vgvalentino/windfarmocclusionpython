# import numpy as np
# from matplotlib import pyplot as plt
#
# from Handler.FineTuningDataHolder import FineTuningDataHolder
# from Depricated import utils
#
#
# class FineTuningSLAM_Scene:
#
#     def __init__(self, dataholder: FineTuningDataHolder):
#         if dataholder.fine_tuning_mode != utils.FineTuneMode.SLAM: raise Exception("Wrong data or wrong mode in FineTuning Class")
#         self.images = dataholder.images
#         self.fprs = np.array(dataholder.fprs)
#         self.fnrs = np.array(dataholder.fnrs)
#         self.thresholds = np.array(dataholder.thresholds)
#
#     def visualize_images(self, only_relevant=False):
#         if only_relevant:
#             prev_fpr, prev_fnr = -1, -1
#             concat = []
#             threshold_texts = []
#             for id, image in enumerate(self.images):
#                 if self.fprs[id] != prev_fpr or self.fnrs[id] != prev_fnr:      # image changed
#                     prev_fpr, prev_fnr = self.fprs[id], self.fnrs[id]
#                     concat.append(image)
#                     threshold_texts.append(f"threshold={self.thresholds[id]:.2f}")
#             return utils.concat_horizontal(concat, threshold_texts)
#         else:
#             threshold_texts = [f"threshold={thresh:.2f}" for thresh in self.thresholds]
#             return utils.concat_horizontal(self.images, threshold_texts)
#
#     def plot_fprs_against_fnrs(self):
#         #%matplotlib inline
#         plt.plot(self.thresholds, self.fprs, 'r', label="FPR")
#         plt.plot(self.thresholds, self.fnrs, 'orange', label="FNR")
#         plt.plot(self.thresholds, self.calculate_scores(), 'pink', label="Score")
#         best_threshold = self.get_best_threshold()
#         plt.axvline(x=best_threshold, color='b', label="best threshold", ymin=0, ymax=1)
#         plt.xlabel("SLAM Threshold")
#         plt.ylabel("rates")
#         plt.legend()
#         plt.show()
#
#     def calculate_scores(self):
#         return 0.6 * self.fprs + 0.4 * self.fnrs
#
#     def get_best_threshold(self):
#         score = self.calculate_scores()
#         indices = np.where(score == score.min())
#         index = indices[0][-1]
#         print(f"index={index} threshold={self.thresholds[index]}")
#         return self.thresholds[index]
#
#
#
# import pandas as pd
#
#
# class FineTuningMono_Sequence:
#
#     def __init__(self, finetuner_dataholders):
#         self.all_images = []
#         self.all_fprs = np.zeros((len(finetuner_dataholders), len(finetuner_dataholders[0].fprs)))
#         self.all_fnrs = np.zeros_like(self.all_fprs)
#         for id, dataholder in enumerate(finetuner_dataholders):
#             if dataholder.fine_tuning_mode != utils.FineTuneMode.MONO: raise Exception("Wrong data or wrong mode in FineTuning Class")
#             self.all_images.append(dataholder.images)
#             self.all_fprs[id] = np.array(dataholder.fprs)
#             self.all_fnrs[id] = np.array(dataholder.fnrs)
#         self.mean_fprs = np.mean(self.all_fprs, axis=0)
#         self.mean_fnrs = np.mean(self.all_fnrs, axis=0)
#         self.topography = np.array(finetuner_dataholders[0].topography)     # all topography lists are the same otherwise the comparison would not make sense
#         self.modes = np.array(finetuner_dataholders[0].modes)               # all mode lists are the same otherwise the comparison would not make sense
#
#         dataframe = pd.DataFrame(columns=["Scene_Number", "Mean_FPR", "Mean_FNR", "Topography", "MonoMode"])
#
#     def visualize_images(self, only_relevant=False, path=""):
#         """Do not return visualization because the needed numpy array would be too large. Instead save each Scene visualization as a file."""
#         configs = [f"topography={topo}\n mode={mode}" for topo, mode in zip(self.topography, self.modes)]
#         concat = []
#         for scene_id, images in enumerate(self.all_images):
#             #cv2.imwrite(f"{path}{scene_id}.jpg", utils.concat_horizontal(images, configs))
#             concat.append(utils.concat_horizontal(images, configs))
#         return utils.concat_vert(concat)
#
#     def plot_fprs_against_fnrs(self, title=None):
#         #%matplotlib inline
#         plt.plot(self.thresholds, self.mean_fprs, 'r', label="Mean FPR")
#         plt.plot(self.thresholds, self.mean_fnrs, 'orange', label="Mean FNR")
#         plt.plot(self.thresholds, self.calculate_scores(self.mean_fprs, self.mean_fnrs), 'pink', label="Score")
#         best_threshold = self.get_best_threshold(self.mean_fprs, self.mean_fnrs)
#         plt.axvline(x=best_threshold, color='b', label=f"Best Threshold = {best_threshold}", ymin=0, ymax=1)
#         x_ticks = [0,10,20,30,40,50,60, best_threshold]#.sort()
#         plt.xticks(x_ticks)
#         if title is not None:
#             plt.title(str(title))
#         plt.xlabel("SLAM Threshold")
#         plt.ylabel("Rate")
#         plt.legend(loc='lower right', bbox_to_anchor=(1,0.2))
#         plt.show()
#
#     def calculate_scores(self, fprs, fnrs):
#         return 0.6 * fprs + 0.4 * fnrs
#
#     def get_best_threshold(self, fprs, fnrs):
#         score = self.calculate_scores(fprs, fnrs)
#         indices = np.where(score == score.min())
#         index = indices[0][-1]
#         print(f"index={index} threshold={self.thresholds[index]}")
#         return self.thresholds[index]
#
#     def plot_thresholds_for_all_scenes(self, title=None):
#         for fprs, fnrs in zip(self.all_fprs, self.all_fnrs):
#             best_thresh = self.get_best_threshold(fprs, fnrs)
#             plt.axvline(x=best_thresh, color='b', ymin=0, ymax=1)
#         if title is not None:
#             plt.title(str(title))
#         plt.xlabel("SLAM Threshold")
#         plt.ylabel("Nothing")
#         plt.show()