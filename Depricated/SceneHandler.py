# import os.path
#
# from keras.models import load_model
# from DenseDepth.layers import BilinearUpSampling2D
# from DenseDepth.utils import predict
# from tqdm import tqdm
#
# import cv2
# import numpy as np
# from Depricated import utils
# from Depricated.RotationHandler import RotationHandler
# from Depricated.utils import Vec2D, Vec2DInt, MonoThresholdMode, ImageVariant
#
#
# class SceneHandler:
#     """
#     This SceneHandler is used to handle all images of the same Scene and create the occluded image and store the intermediate computation results for analysis.
#     It is initialized once and after that it can load all Scenes out of the Scenefolder using the image_number and load_scene() function
#     All images have the shape (height, width, 3) except the occlusion_maps which have the shape (height, width)
#     The depth_maps also have the shape (height, width, 3) but only the first channel contains the depth values. This is done for simplicity when working with
#     opencv.
#     """
#
#     def __init__(self, path_prefix, image_format=".png", model="DenseDepth/nyu.h5"):
#         """
#         Initializes a SceneHandler. After that a scene can be loaded with the load_scene function.
#         :param path_prefix: path to the scene data.
#         :param image_format: format of the images.
#         :param model: either a string representing the path to the model which shall be loaded or an already loaded model.
#         """
#         self.windturbine_distance = 0
#         self.path_prefix = path_prefix
#         self.image_format = image_format
#
#         if type(model) == str:
#             self.model = load_model(model, custom_objects={'BilinearUpSampling2D': BilinearUpSampling2D, 'depth_loss_function': None}, compile=False)
#             print('\nModel loaded ({0}).'.format(self.model))
#         else:
#             self.model = model
#
#         self.scene_number = None
#         self.imageHandler = None
#         self.monocular_depth_map = None
#         self.topo_occlusion_map, self.slam_occluison_map, self.mono_occlusion_map = None, None, None
#         self.global_occlusion_map = None
#         self.occluded_image = None
#
#     def load_scene(self, image_number, use_topography=True, mono_threshold_mode=MonoThresholdMode.MAX, slam_treshold_distance=10, fastloading=False):
#         # load everything
#         self.scene_number = image_number
#         screen_image, backround_image, topo_depth, topo_depth_no_buildings, slam_depth = self.__load_images(self.path_prefix, self.image_format, image_number)
#         points, self.windturbine_distance = self.__load_points(self.path_prefix, image_number, screen_image)
#
#         # rotate and cutout everything with a new ImageHandler
#         self.imageHandler = RotationHandler(screen_image, points)
#         self.imageHandler.rotate_and_cut(backround_image)
#         self.imageHandler.rotate_and_cut(topo_depth)
#         self.imageHandler.rotate_and_cut(topo_depth_no_buildings)
#         self.imageHandler.rotate_and_cut(slam_depth)
#
#         self.monocular_depth_map = self.__get_monocular_depth_map(self.getBackground()[-1], self.path_prefix, image_number, fastloading=fastloading)
#
#         # Get Occlusion Maps and occlude image with global_occlusion_map
#         self.set_occlusion_maps(use_topography, mono_threshold_mode, slam_treshold_distance)
#
#     def init_sky_segmentation_for_occlusion(self):
#         segmentation, _, _ = self.get_sky_segmentation()
#         self.imageHandler.rotate_and_cut(segmentation)
#
#     def get_sky_segmentation(self):
#         back = self.getBackground()[ImageVariant.FULL]
#         points = self.imageHandler.input_points
#         sky_mask = utils.get_sky_mask(back, points)
#         ground_mask = self.getTopo()[ImageVariant.FULL][:, :, 0]
#         ground_mask = ground_mask > 1 / self.windturbine_distance
#         segmentation = self.k_means(back, back[sky_mask], back[ground_mask])
#         segmentation = np.stack([segmentation, segmentation, segmentation], axis=2)
#         return segmentation, sky_mask, ground_mask
#
#     def k_means(self, image, sky_init_pixels, ground_init_pixels):
#         #start = time.time()
#         small_image = cv2.resize(image, (120, 160))
#         data = small_image.reshape((-1, small_image.shape[2]))
#         sky_center = np.mean(sky_init_pixels, axis=0)
#         ground_init_pixels = ground_init_pixels.reshape((-1, ground_init_pixels.shape[-1]))
#         ground_center = np.mean(ground_init_pixels, axis=0)
#         distances = np.linalg.norm(np.abs(ground_init_pixels - ground_center), axis=1)
#         last_10_percent = np.argsort(distances)[int(0.9 * ground_init_pixels.shape[0]):ground_init_pixels.shape[0]]
#         second_ground_center = np.mean(ground_init_pixels[last_10_percent], axis=0)
#
#         sky_distance = np.linalg.norm(np.abs(data - sky_center), axis=1)
#         ground_distance = np.linalg.norm(np.abs(data - ground_center), axis=1)
#         second_ground_distance = np.linalg.norm(np.abs(data - second_ground_center), axis=1)
#
#         labels = np.zeros(data.shape[0], dtype=bool)
#         labels[np.logical_and(sky_distance < ground_distance, sky_distance < second_ground_distance)] = 1
#
#         # sky_center = np.mean(data[labels], axis=0)
#         # ground_center = np.mean(data[labels==0], axis=0)
#         # sky_distance = np.linalg.norm(np.abs(data-sky_center), axis=1)
#         # ground_distance = np.linalg.norm(np.abs(data-ground_center), axis=1)
#         #
#         # labels = np.zeros(data.shape[0], dtype=bool)
#         # labels[sky_distance <= ground_distance] = 1
#         #print(time.time() - start)
#         labels = labels.reshape((small_image.shape[0], small_image.shape[1]))
#         labels = cv2.resize(labels.astype(np.uint8), (image.shape[1], image.shape[0]), interpolation=cv2.INTER_NEAREST)
#         segmentation = labels.astype(bool)
#         return segmentation
#
#     def set_occlusion_maps(self, use_topography: bool, mono_threshold_mode: MonoThresholdMode, slam_treshold_distance=10, with_buildings=True):
#         """Helper function to set the occlusion maps the occluded image. Can also be used from outside to compare different thresholding techniques."""
#         if not with_buildings:
#             topo_depth = self.getTopoNoBuildings()[ImageVariant.CUTOUT]
#         else:
#             topo_depth = self.getTopo()[ImageVariant.CUTOUT]
#         self.topo_occlusion_map, self.slam_occluison_map, self.mono_occlusion_map = \
#             self.__get_occlusion_maps(topo_depth, self.getSLAM()[ImageVariant.CUTOUT], self.monocular_depth_map, self.windturbine_distance, use_topography,
#                                       mono_threshold_mode, slam_treshold_distance)
#         self.global_occlusion_map = np.logical_or(np.logical_or(self.topo_occlusion_map, self.slam_occluison_map), self.mono_occlusion_map)
#         self.occluded_image = self.__occlude(self.getBackground()[-1], self.getScreen()[-1], self.global_occlusion_map)
#
#     def __occlude(self, foreground, background, occlusion_map):
#         """Occlude the background with foreground at pixels where occlusion_map is True"""
#         result = background.copy()
#         result[occlusion_map] = foreground[occlusion_map]
#         return result
#
#     def __get_monocular_depth_map(self, image, path_prefix, image_number, fastloading=False):
#         """Using the deep learning network to compute a monocular depth map if none can be loaded from disk."""
#         if fastloading:
#             try:
#                 return np.load(f"{path_prefix}mono_{image_number}.npy")
#             except FileNotFoundError:
#                 pass
#         inputs = np.array([cv2.resize(image, (640, 480))]) / 255  # Adjust input image to fit model
#         output = predict(self.model, inputs)[0]
#         monocular_depth_map = cv2.resize(output.astype(np.float32), (image.shape[1], image.shape[0]))  # Adjust output to fit background_cutout
#         monocular_depth_map = np.dstack((monocular_depth_map, np.zeros((*monocular_depth_map.shape[:2], 2))))  # Adjust output to fit cv2
#         np.save(f"{path_prefix}mono_{image_number}.npy", monocular_depth_map)
#         return monocular_depth_map
#
#     def __get_occlusion_maps(self, topoDepth, slamDepth, monoDepth, windturbine_distance, use_topography: bool, mono_threshold_mode,
#                              slam_threshold_distance=10):
#         """
#         Gets the occlusion maps using the given depth maps.
#         By changing the slam_threshold_distance the quality of the slam_occlusion_map can be adjusted. (Used for Fine-tuning)
#         By changing the mono_threshold_mode and the use_topography boolean the quality of the mono_occlusion_map can be adjusted. (Used for Fine-tuning)
#         """
#         topo_occlusion_map = self.__get_topo_occlusion_map(topoDepth, windturbine_distance)
#         slam_occlusion_map = self.__get_slam_occlusion_map(slamDepth, windturbine_distance, slam_threshold_distance)
#         mono_occlusion_map = self.get_mde_occlusion_map(monoDepth, use_topography, mono_threshold_mode, topo_occlusion_map)
#         # cv2.resize(np.dstack([occlusion_map, occlusion_map, occlusion_map]).astype(np.uint8), (self.screen_cutout.shape[1], self.screen_cutout.shape[0]))
#         return topo_occlusion_map, slam_occlusion_map, mono_occlusion_map
#
#     def __get_topo_occlusion_map(self, topo_depth, windturbine_distance):
#         topo_occlusion_map = topo_depth[:, :, 0]  > 1 / windturbine_distance # topodepth saved as 1/distance
#         return topo_occlusion_map
#
#     def __get_slam_occlusion_map(self, slam_depth, windturbine_distance, slam_threshold_distance=10):
#         slam_occlusion_map = slam_depth[:, :, 0] < windturbine_distance  # slamdepth saved in meters - True if pixel is in front of Scene-Object
#         slam_occlusion_map[slam_depth[:, :, 0] >= slam_threshold_distance] = False  # the higher the slam_distance the lower the confidence
#         return slam_occlusion_map
#
#     def get_mde_occlusion_map(self, mono_depth, use_topography: bool, mono_threshold_mode, topo_occlusion_map=None):
#         if use_topography:
#             values = mono_depth[topo_occlusion_map, 0]
#         else:
#             point = Vec2DInt(y=(0.8 * self.monocular_depth_map.shape[0]), x=(0.5 * self.monocular_depth_map.shape[1]))  # TODO use given points
#             values = mono_depth[point.y:, point.x - 1:point.x + 2, 0]
#
#         if mono_threshold_mode == MonoThresholdMode.UF:
#             mono_threshold = utils.uf_range_get_mono_threshold(values)
#         elif mono_threshold_mode == MonoThresholdMode.QUARTILE3:
#             mono_threshold = utils.quartil3_get_mde_threshold(values)
#         elif mono_threshold_mode == MonoThresholdMode.MIN_UF_MAX:
#             mono_threshold = utils.min_uf_max_range_get_mono_threshold(values)
#         elif mono_threshold_mode == MonoThresholdMode.HAND:  # Use User to define Threshold or use old threshold which was defined by a user in the past
#             hand_threshold_path = f"{self.path_prefix}hand_threshold_{self.scene_number}.txt"
#             if os.path.exists(hand_threshold_path):
#                 with open(hand_threshold_path, "r") as file:
#                     mono_threshold = float(file.readline())
#             else:
#                 mono_threshold = utils.hand_get_mde_threshold(mono_depth, utils.draw_mask_on_image(self.getBackground()[ImageVariant.CUTOUT],
#                                                                                                    self.imageHandler[-1][ImageVariant.CUTOUT][:, :, 0],
#                                                                                                    color=(0, 0, 255)))
#                 with open(hand_threshold_path, "w") as file:
#                     file.write(f"{mono_threshold}")
#         else:  # MonoThresholdMode.MAX:
#             mono_threshold = utils.default_get_mde_threshold(values)
#
#         mono_occlusion_map = mono_depth[:, :, 0] < mono_threshold
#         return mono_occlusion_map
#
#     @staticmethod
#     def __load_images(path_prefix, image_format=".png", image_number=0):
#         """Loads all needed images and makes sure that they all have the same shape"""
#         # Load Scene Images
#         screen_image = cv2.imread(f"{path_prefix}screen_{image_number}{image_format}")
#         background_image = cv2.imread(f"{path_prefix}background_{image_number}{image_format}")  # because of problem with saving images
#         # Adjust Scene Images to each other
#         background_image = cv2.rotate(background_image, cv2.ROTATE_90_COUNTERCLOCKWISE)
#         sH, sW, _ = screen_image.shape
#         bH, bW, _ = background_image.shape
#         screenRatio = sW / sH
#         need_to_cut_away = int((bW - bH * screenRatio) // 2)
#         background_image = background_image[:, need_to_cut_away:bW - need_to_cut_away]
#         globalH, globalW = background_image.shape[:2]
#         screen_image = cv2.resize(screen_image, (globalW, globalH))
#
#         # Load Depth Images from .txt files containing either float16 or float32 depth values and adjusting them to the Scene Images
#         topo_depth = np.flip(np.fromfile(f"{path_prefix}topo_{image_number}.txt", dtype=np.float16), axis=0).astype(np.float32)  # depth values in 1/distance
#         topo_depth = topo_depth.reshape((sH, sW, -1))
#         topo_depth = cv2.flip(topo_depth, 1)
#         topo_depth = cv2.resize(topo_depth, (globalW, globalH))
#         topo_depth = np.dstack((topo_depth, np.zeros((*topo_depth.shape[:2], 2))))
#
#         try:
#             topo_depth_no_buildings = np.flip(np.fromfile(f"{path_prefix}topo_no_building_{image_number}.txt", dtype=np.float16), axis=0).astype(np.float32)  # depth values in 1/distance
#         except FileNotFoundError:
#             topo_depth_no_buildings = np.flip(np.fromfile(f"{path_prefix}topo_{image_number}.txt", dtype=np.float16), axis=0).astype(np.float32)
#         topo_depth_no_buildings = topo_depth_no_buildings.reshape((sH, sW, -1))
#         topo_depth_no_buildings = cv2.flip(topo_depth_no_buildings, 1)
#         topo_depth_no_buildings = cv2.resize(topo_depth_no_buildings, (globalW, globalH))
#         topo_depth_no_buildings = np.dstack((topo_depth_no_buildings, np.zeros((*topo_depth_no_buildings.shape[:2], 2))))
#
#         slam_depth = np.fromfile(f"{path_prefix}slam_{image_number}.txt", dtype=np.float32)  # depht values in meters
#         slam_depth = slam_depth.reshape((120, 160))
#         slam_depth = cv2.flip(slam_depth, 1)
#         slam_depth = cv2.rotate(slam_depth, cv2.ROTATE_90_CLOCKWISE)
#         slam_depth = cv2.resize(slam_depth, (bW, bH))  # contains the same content as the background image (need to cut away)
#         slam_depth = slam_depth[:, need_to_cut_away:bW - need_to_cut_away]
#         slam_depth = np.dstack((slam_depth, np.zeros((*slam_depth.shape[0:2], 2))))
#
#         assert (screen_image.shape == background_image.shape and screen_image.shape == topo_depth.shape and screen_image.shape == slam_depth.shape)
#         return screen_image, background_image, topo_depth, topo_depth_no_buildings, slam_depth
#
#     @staticmethod
#     def __load_points(path_prefix, image_number, screen_image):  # TODO until now it does not make sure that the points are in the image
#         """Loads points from pointfile and returns points scaled according to the screen_image. Also extracts the wind turbine distance."""
#         with open(f"{path_prefix}points_{image_number}.txt", "r") as file:
#             line = file.readline().replace("(", "", -1).replace(")", "", -1)
#             values = line.split(",")
#             rel_points = [Vec2D(x=float(values[3]), y=float(values[4])), Vec2D(x=float(values[0]), y=float(values[1]))]
#             windturbine_distance = min(float(values[2]), float(values[5]))
#             # Adjust points to different coordinate system
#             rel_points[0].y = 1 - rel_points[0].y
#             rel_points[1].y = 1 - rel_points[1].y
#             points = utils.convertRelPointsToAbsPoints(rel_points, *screen_image.shape[0:2])
#         return points, windturbine_distance
#
#     """Those functions are used to access the different images which were producted by the imageHandler more efficiently.
#     Each function returns the input_image, square_image, rotated_image and cutout_image <- can be accessed with utils.ImageVariant"""
#
#     def getScreen(self):
#         return self.imageHandler[0]
#
#     def getBackground(self):
#         return self.imageHandler[1]
#
#     def getTopo(self):
#         return self.imageHandler[2]
#
#     def getTopoNoBuildings(self):
#         return self.imageHandler[3]
#
#     def getSLAM(self):
#         return self.imageHandler[4]
