# import cv2
# import numpy as np
# from tqdm import tqdm
#
# from Depricated import utils
# from Depricated.FineTuningDataHolder import FineTuningDataHolder
# from Depricated.utils import ImageVariant
# from Depricated.SceneHandler import SceneHandler
#
#
# class AnalysisHandler:
#     """
#     The AnalysisHandler is a wrapper around an already initialized SceneHandler. After the SceneHandler has loaded a scene the AnalysisHandler can be used
#     for analizing the intermediate computation results.
#     """
#
#     def __init__(self, sceneHandler: SceneHandler, image_number):
#         """
#         :param sceneHandler: An already initialized ScreenHandler which also already has loaded a Scene.
#         :param image_number: The image_number of the loaded Scene in order to load the ground_truth.
#         """
#         self.path_prefix = sceneHandler.path_prefix
#         self.sceneHandler = sceneHandler
#         groundtruth_occusion_map = self.__load_ground_truth(sceneHandler.path_prefix, image_number, sceneHandler.image_format)
#         self.sceneHandler.imageHandler.rotate_and_cut(groundtruth_occusion_map)
#         self.groundtruth_variants = self.sceneHandler.imageHandler[-1]
#         self.groundtruth_occlusion_map = self.groundtruth_variants[ImageVariant.CUTOUT][:, :, 0] / 255
#
#     def __load_ground_truth(self, path_prefix, image_number, image_format):
#         """Since the ground truth image is not needed inside the SceneHandler it is loaded in this AnalysisHandler class."""
#         groundtruth_image = cv2.imread(f"{path_prefix}groundtruth_{image_number}{image_format}")
#         if groundtruth_image is None:
#             raise FileNotFoundError("Groundtruth image does not exist")
#         groundtruth_image = cv2.rotate(groundtruth_image, cv2.ROTATE_90_COUNTERCLOCKWISE)
#         sH, sW, _ = self.sceneHandler.getScreen()[ImageVariant.FULL].shape
#         bH, bW, _ = groundtruth_image.shape
#         screenRatio = sW / sH
#         need_to_cut_away = int((bW - bH * screenRatio) // 2)
#         groundtruth_image = groundtruth_image[:, need_to_cut_away:bW - need_to_cut_away]
#         assert ((sH, sW) == groundtruth_image.shape[:2])
#         return groundtruth_image
#
#     def visualize_occlusion_masks(self):
#         """Visualizes all occlusion maps by masking them on the cutout screen image and then putting the masked images side by side."""
#         image = self.sceneHandler.getScreen()[-1]
#         occlusion_maps = self.get_occlusion_maps()
#         concat = []
#         for occl_map in occlusion_maps:
#             concat.append(utils.draw_mask_on_image(image, occl_map))
#         result = utils.concat_horizontal(concat)
#         return result
#
#     def visualize_occlusion_maps(self):
#         """Visualizes all occlusion maps as a binary image, putting the images side by side. Also displays the occluded image."""
#         occlusion_maps = self.get_occlusion_maps()
#         concat = [occ_map * 255 for occ_map in occlusion_maps]
#         concat.append(self.sceneHandler.occluded_image)
#         result = utils.concat_horizontal(concat)
#         return result
#
#     def visualize_all_full_images(self):
#         """Visualizes all input images, putting them side by side. Will visualize less images since the occluded_image and monocular_depth_map are only
#         computed using the cutout_images and have therefore not the full resolution."""
#         id = ImageVariant.FULL
#         result = utils.concat_horizontal(
#             [self.sceneHandler.getScreen()[id], self.sceneHandler.getBackground()[id], self.groundtruth_variants[id], utils.rescale_depth_images(self.sceneHandler.getSLAM()[id]),
#              utils.rescale_depth_images(self.sceneHandler.getTopo()[id])])
#         return result
#
#     def visualize_all_cutout_images(self):
#         """Visualizes all cutout images, putting them side by side."""
#         id = ImageVariant.CUTOUT
#         result = utils.concat_horizontal([self.sceneHandler.getScreen()[id], self.sceneHandler.getBackground()[id], self.sceneHandler.occluded_image,
#                                           self.groundtruth_variants[id], utils.rescale_depth_images(self.sceneHandler.getSLAM()[id]),
#                                           utils.rescale_depth_images(self.sceneHandler.getTopo()[id]), utils.rescale_depth_images(self.sceneHandler.monocular_depth_map)])
#         return result
#
#     def visualize_missclassification(self):
#         """
#         Uses the visualize_comparison_with_groundtruth in order to draw the misclassifications and putting them side by side. Also displays the occluded image.
#         Also returns the false_positive and false_negative rates for all occlusion maps. 0=groundtruth 1=topo 2=slam 3=mono 4=global.
#         """
#         occlusion_maps = self.get_occlusion_maps()
#         concat = []
#         fprs = []
#         fnrs = []
#         for occl_map in occlusion_maps:
#             image, fpr, fnr = self.visualize_comparison_with_groundtruth(occlusion_maps[0], occl_map)
#             concat.append(image)
#             fprs.append(fpr)
#             fnrs.append(fnr)
#         concat.append(self.sceneHandler.occluded_image)
#         return utils.concat_horizontal(concat), fprs, fnrs
#
#     def __get_misclassified_rates(self, groundtruth, inference):
#         """false_positive: too much occluded; false_negative: not enough occluded"""
#         if len(groundtruth.shape) == 3:
#             groundtruth = groundtruth[:, :, 0]
#         positive = np.count_nonzero(inference)
#         negative = inference.shape[0] * inference.shape[1] - positive
#
#         true_positive = np.count_nonzero(np.logical_and(groundtruth, inference))
#         true_negative = np.count_nonzero(np.logical_and(np.logical_not(groundtruth), np.logical_not(inference)))
#
#         false_positive = positive - true_positive
#         false_negative = negative - true_negative
#
#         false_positive_rate = 0 if false_positive == 0 else false_positive / (false_positive + true_negative)
#         false_negative_rate = 0 if false_negative == 0 else false_negative / (false_negative + true_positive)
#
#         return false_positive_rate, false_negative_rate
#
#     def visualize_comparison_with_groundtruth(self, groundtruth, inference):
#         """
#         Compares two occlusion_masks: the groundtruth with the inferred one. Each pixel is colored in Green, Yellow or Red. Additionally, the False Positive and
#         False Negative Rate are drawn on the images.
#         If a pixel is green: correct occlusion.      If a pixel is yellow: pixel should be occluded but the inferred occlusion map says different.
#         If a pixel is red: pixel should not be occluded but the inferred occlusion map says different.
#         """
#         result = np.zeros((groundtruth.shape[0], groundtruth.shape[1], 3))
#         result[np.logical_and(groundtruth, inference)] = (0, 252, 124)  # correct
#         result[np.logical_and(np.logical_not(groundtruth), inference)] = (0, 0, 255)  # wrong occluded
#         result[np.logical_and(groundtruth, np.logical_not(inference))] = (0, 252, 255)  # wrong not occluded
#         fpr, fnr = self.__get_misclassified_rates(groundtruth, inference)
#         result = utils.write_text_on_image(result, f"FPR={fpr:.4f}, FNR={fnr:.4f}", text_layer=0)
#         return result, fpr, fnr
#
#     def get_occlusion_maps(self):
#         """Used to easily acess the occlusion maps."""
#         occlusion_maps = [self.groundtruth_occlusion_map, self.sceneHandler.topo_occlusion_map, self.sceneHandler.slam_occluison_map,
#                                self.sceneHandler.mono_occlusion_map, self.sceneHandler.global_occlusion_map]  # used to make the occlusion maps easier to access
#         return occlusion_maps
#
#     def finetune_slam_threshold(self):
#         """Try out different slam_thresholds from 0-65 and compare the results with the groundtruth occlusion map. Compute out of this comparison the FPR and
#         FNR for every tried threshold."""
#         slam_depth = self.sceneHandler.getSLAM()[ImageVariant.FULL][:, :, 0]
#
#         thresholds = [65/100 * val for val in range(1, 101, 3)]
#         thresholds = [val for val in range(0, 66)]
#         data_holder = FineTuningDataHolder(utils.FineTuneMode.SLAM)
#         for threshold in tqdm(thresholds, total=len(thresholds), desc=f"Trying out different Thresholds for Scene {self.sceneHandler.scene_number}", leave=True, position=0):
#             slam_occlusion_map = np.array(slam_depth < self.sceneHandler.windturbine_distance)  # slamdepth saved in meters - True if pixel is in front of Scene-Object
#             slam_occlusion_map[slam_depth >= threshold] = False  # the higher the slam_distance the lower the confidence
#             self.get_occlusion_maps()
#             image, fpr, fnr = self.visualize_comparison_with_groundtruth(self.groundtruth_variants[ImageVariant.FULL][:, :, 0]/255, slam_occlusion_map)
#
#             data_holder.add_data_sfm(image, fpr, fnr, threshold)
#         return data_holder
#
#     def finetune_mono_threshold_mode(self):
#         """Try out different mono_threhold modes. In the first for loop the topography is not used and all modes are tried. In the second for loop the modes use
#         the topography as region selection to find the threshold."""
#         data_holder = FineTuningDataHolder(utils.FineTuneMode.MONO)
#         occlusion_maps = self.get_occlusion_maps()
#         for mode in utils.MonoThresholdMode:
#             self.sceneHandler.set_occlusion_maps(use_topography=False, mono_threshold_mode=mode, slam_treshold_distance=0)
#             self.get_occlusion_maps()
#             image, fpr, fnr = self.visualize_comparison_with_groundtruth(occlusion_maps[0], occlusion_maps[3])
#             if mode != utils.MonoThresholdMode.HAND:
#                 data_holder.add_data_mde(image, fpr, fnr, topography=False, mode=mode)
#         for mode in utils.MonoThresholdMode:
#             self.sceneHandler.set_occlusion_maps(use_topography=True, mono_threshold_mode=mode, slam_treshold_distance=0)
#             self.get_occlusion_maps()
#             image, fpr, fnr = self.visualize_comparison_with_groundtruth(occlusion_maps[0], occlusion_maps[3])
#
#             data_holder.add_data_mde(image, fpr, fnr, topography=True, mode=mode)
#         return data_holder
#
#     def finetune_slam_threshold_with_sky_segmentation(self):
#         """Try out different slam_thresholds from 0-65 combined with sky segmentationg and compare the results with the groundtruth occlusion map. Compute out
#         of this comparison the FPR and FNR for every tried threshold."""
#         slam_depth = self.sceneHandler.getSLAM()[ImageVariant.FULL][:, :, 0]
#
#         thresholds = [val for val in range(0, 66)]
#         data_holder = FineTuningDataHolder(utils.FineTuneMode.SLAM)
#         sky_occlusion_map, _, _ = self.sceneHandler.get_sky_segmentation()
#         sky_occlusion_map = sky_occlusion_map[:,:,0]
#         for threshold in tqdm(thresholds, total=len(thresholds), desc=f"Trying out different Thresholds for Scene {self.sceneHandler.scene_number}", leave=False,
#                               position=0):
#             slam_occlusion_map = np.array(slam_depth < self.sceneHandler.windturbine_distance)  # slamdepth saved in meters - True if pixel is in front of Scene-Object
#             slam_occlusion_map[slam_depth >= threshold] = False  # the higher the slam_distance the lower the confidence
#             slam_occlusion_map[sky_occlusion_map] = False       # set sky pixels to background
#             image, fpr, fnr = self.visualize_comparison_with_groundtruth(self.groundtruth_variants[ImageVariant.FULL][:, :, 0] / 255, slam_occlusion_map)
#
#             data_holder.add_data_sfm(image, fpr, fnr, threshold)
#         return data_holder