# from enum import Enum
#
# from keras.models import load_model
# from DenseDepth.layers import BilinearUpSampling2D
# from tensorflow.keras.layers import Layer, InputSpec
# from DenseDepth.utils import predict, load_images, display_images
# import numpy as np
# import matplotlib.pyplot as plt
#
# import cv2
#
# from Depricated import utils
# from Depricated.SceneHandler import SceneHandler
# from Depricated.utils import Vec2D, Vec2DInt, ImageVariant
#
#
# class OcclusionHandler:
#
#     def __init__(self, sceneHandler :SceneHandler):
#         self.scene = sceneHandler
#         pass
#
#     def occlude_cutout_scene(self):
#         foreground = self.scene.getScreen()[ImageVariant.CUTOUT]
#         background = self.scene.getBackground()[ImageVariant.CUTOUT]
#
#
#     @staticmethod
#     def occlude_image(cv2_foreground_image, cv2_background_image, occlusion_mask):
#         assert cv2_foreground_image.shape == cv2_background_image.shape and \
#                cv2_foreground_image.shape == occlusion_mask.shape, "OcclusionHandler occlude_image shape mismatch"
#         result = cv2_background_image.copy()
#         result[occlusion_mask] = cv2_foreground_image[occlusion_mask]
#         return result
#
#     # def show_occlusion_sequence(self, image_number_start: int, image_number_end: int, save_path:str="", fps=10, cutout=True):
#     #     running = True
#     #     first_run = True
#     #     video = None
#     #     while(running):
#     #         for i in range(image_number_start,image_number_end+1):
#     #             self.occlusion(i)
#     #             if cutout:
#     #                 frame = self.result_cutout
#     #             else:
#     #                 frame = self.result
#     #             cv2.imshow("Frame", frame)
#     #             if save_path != "" and first_run:
#     #                 if video is None:
#     #                     h,w,_ = frame.shape
#     #                     video = cv2.VideoWriter(f'{save_path}.avi', cv2.VideoWriter_fourcc(*'MJPG'), fps, (w, h))
#     #                 if frame.shape[:2] != (h,w):
#     #                     frame = cv2.resize(frame, (w,h))
#     #                 video.write(frame)
#     #
#     #             if cv2.waitKey(60) & 0xFF == ord('q'):
#     #                 running = False
#     #                 break
#     #         first_run = False
#     #     cv2.destroyAllWindows()
#     #
#     # def show_occlusion_sequence_with_predefined_occlusion_map(self, image_number_start: int, image_number_end: int, occlusion_map_number: int, save_path:str="", fps=10):
#     #     running = True
#     #     first_run = True
#     #     video = None
#     #     occlusion_map = self.get_occlusion_map(occlusion_map_number)
#     #     while (running):
#     #         for i in range(image_number_start, image_number_end + 1):
#     #             self.occlusion(i)
#     #             self.occlude(occlusion_map)
#     #             frame = self.result_cutout
#     #             cv2.imshow(f"Frame", frame)
#     #             if save_path != "" and first_run:
#     #                 if video is None:
#     #                     h, w, _ = frame.shape
#     #                     video = cv2.VideoWriter(f'{save_path}.avi', cv2.VideoWriter_fourcc(*'MJPG'), fps, (w, h))
#     #                 if frame.shape[:2] != (h, w):
#     #                     frame = cv2.resize(frame, (w, h))
#     #                 video.write(frame)
#     #
#     #             if cv2.waitKey(60) & 0xFF == ord('q'):
#     #                 running = False
#     #                 break
#     #         first_run = False
#     #     cv2.destroyAllWindows()